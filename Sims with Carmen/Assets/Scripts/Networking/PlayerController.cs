﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

    public static PlayerController mine { get; private set; }

    #region Variables
    private WallTool m_wallTool;
    private FoundationTool m_foundationTool;
    private TerrainTest m_terrainTool;
    #endregion

    #region Methods
    #region Unity
    // Use this for initialization
    void Start () {
        if (gameObject.Equals(NetworkClient.allClients[0].connection.playerControllers[0].gameObject))
            mine = this;

        m_wallTool = FindObjectOfType<WallTool>();
        m_foundationTool = FindObjectOfType<FoundationTool>();
        m_terrainTool = FindObjectOfType<TerrainTest>();
    }

    private void OnDestroy()
    {
        if (mine == this)
            mine = null;
    }
    #endregion

    #region Main Menu
    // TODO check if works with multiple clients
    [ClientRpc]
    public void RpcSendServerDate(int a_neighbourhoodIndex, int a_lotIndex, long a_serverFileTime, string a_data)
    {
        if (isServer)
            return;
        string path = MenuUIManager.CreatePathFrom(a_neighbourhoodIndex, a_lotIndex);
        if (File.Exists(path))
        {
            long writeTime = File.GetLastWriteTimeUtc(path).ToFileTimeUtc();
            if (writeTime > a_serverFileTime) { CmdLotSyncReply(File.ReadAllText(path)); } // Newer
            else
            {
                File.WriteAllText(path, a_data);
                CmdLotSynced();
            }
        }
        else { File.WriteAllText(path, a_data); }
    }

    [Command]
    private void CmdLotSynced()
    {
        NetworkManager.singleton.ServerChangeScene("LotScene");
    }

    [Command]
    private void CmdLotSyncReply(string a_data)
    {
        string path = MenuUIManager.CreatePathFrom(MenuUIManager.Instance.selectedNeighborhoodId, MenuUIManager.Instance.selectedLotId);
        File.WriteAllText(path, a_data);
        NetworkManager.singleton.ServerChangeScene("LotScene");
    }
    #endregion

    #region Build Mode
    [Command]
    public void CmdPlaceWall(string a_indices, int a_direction, int a_floorIndex, int a_subFloorIndex)
    {
        // TODO: Rpc should only be send to the user which doesn't have the walls
        RpcPlaceWall(a_indices, a_direction, a_floorIndex, a_subFloorIndex);
    }

    [ClientRpc]
    private void RpcPlaceWall(string a_indices, int a_direction, int a_floorIndex, int a_subFloorIndex)
    {
        if (m_wallTool != null)
        {
            m_wallTool.ActionPlaceWall(a_indices, a_direction, a_floorIndex, a_subFloorIndex);
        }
    }

    [Command]
    public void CmdPlaceFoundation(string a_indices, int a_tool)
    {
        RpcPlaceFoundation(a_indices, a_tool);
    }

    [ClientRpc]
    private void RpcPlaceFoundation(string a_indices, int a_tool)
    {
        if (isLocalPlayer)
            return;
        if (m_foundationTool != null)
            m_foundationTool.ActionPlaceFoundation(a_indices, a_tool);
    }

    [Command]
    public void CmdLevelTerrain(Vector3 a_startPoint, Vector3 a_endPoint)
    {
        RpcLevelTerrain(a_startPoint, a_endPoint);
    }

    [ClientRpc]
    private void RpcLevelTerrain(Vector3 a_startPoint, Vector3 a_endPoint)
    {
        if (m_terrainTool != null) { m_terrainTool.LevelTerrain(a_startPoint, a_endPoint); }
    }

    [Command]
    public void CmdRaiseTerrain(Vector3 a_point)
    {
        RpcRaiseTerrain(a_point);
    }

    [ClientRpc]
    private void RpcRaiseTerrain(Vector3 a_point)
    {
        if (m_terrainTool != null) { m_terrainTool.RaiseTerrain(a_point); }
    }

    [Command]
    public void CmdLowerTerrain(Vector3 a_point)
    {
        RpcLowerTerrain(a_point);
    }

    [ClientRpc]
    private void RpcLowerTerrain(Vector3 a_point)
    {
        if (m_terrainTool != null) { m_terrainTool.LowerTerrain(a_point); }
    }
    #endregion

    #region Buy Mode
    [Command]
    public void CmdPlaceObject(int a_objectId, int a_floorIndex, Vector3 a_location, Quaternion a_rotation)
    {
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        RpcPlaceObject(a_objectId, a_floorIndex, a_location, a_rotation, PlacementController.GenerateUniqueId(floor));
    }

    [ClientRpc]
    private void RpcPlaceObject(int a_objectId, int a_floorIndex, Vector3 a_location, Quaternion a_rotation, int a_unqiueId)
    {
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        FindObjectOfType<PlacementController>().PlaceObject(a_objectId, floor, a_location, a_rotation, a_unqiueId);
    }

    [Command]
    public void CmdReplaceObject(uint a_senderId, int a_floorIndex, int a_uniqueId)
    {
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        RpcReplaceObject(a_senderId, a_floorIndex, a_uniqueId);
    }

    [ClientRpc]
    private void RpcReplaceObject(uint a_senderId, int a_floorIndex, int a_uniqueId)
    {
        if (isLocalPlayer)
            return;
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        FindObjectOfType<PlacementController>().DeleteObject(floor, a_uniqueId);
    }

    [Command]
    public void CmdDeleteObject(int a_floorIndex, int a_uniqueId)
    {
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        RpcDeleteObject(a_floorIndex, a_uniqueId);
    }

    [ClientRpc]
    private void RpcDeleteObject(int a_floorIndex, int a_uniqueId)
    {
        Floor floor = LotManager.Instance.GetFloor(a_floorIndex);
        FindObjectOfType<PlacementController>().DeleteObject(floor, a_uniqueId);
    }
    #endregion
    #endregion
}
