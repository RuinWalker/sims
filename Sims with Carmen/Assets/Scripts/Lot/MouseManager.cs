﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : CachedBehaviour<MouseManager> {

    #region Variables
    #region Static
    public static RaycastHit s_castInfo { get { return Instance.castInfo; } }
    public static Vector3 s_roundedPosition { get { return Instance.roundedPosition; } }
    public static GameObject s_hitObject { get { return Instance.m_hitObject; } }
    public static RaycastHit s_castInfoFloor { get { return Instance.m_castInfoFloor; } }
    public static Vector3 s_tilePositionFloor { get { return Instance.tilePositionFloor; } }
    public static Vector3 s_roundedPositionFloor { get { return Instance.roundedPositionFloor; } }
    public static Floor s_floor { get { return Instance.floor; } }
    public static int s_subFloorIndex { get { return Instance.subFloorIndex; } }
    #endregion

    #region Editor
    [SerializeField]
    private bool m_useMainCamera = true;
    [SerializeField]
    private Camera m_camera;

    [SerializeField]
    private GameObject m_pointer;
    #endregion

    #region Public
    public new Camera camera
    {
        get
        {
            return m_useMainCamera ? Camera.main : m_camera;
        }
    }

    public Vector3 mouseDir
    {
        get
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 1;
            return (camera.ScreenToWorldPoint(mousePos) - camera.transform.position).normalized;
        }
    }

    public RaycastHit castInfo
    {
        get
        {
            CheckCast();
            return m_castInfo;
        }
    }

    public Vector3 roundedPosition
    {
        get
        {
            CheckCast();
            return m_roundedPosition;
        }
    }

    /// <summary>
    /// The object underneath the mouse, if any. Returns null if none.
    /// </summary>
    public GameObject hitObject
    {
        get
        {
            CheckCast();
            return m_hitObject;
        }
    }

    public RaycastHit castInfoFloor
    {
        get
        {
            CheckCast();
            return m_castInfoFloor;
        }
    }

    /// <summary>
    /// The position of the mouse on the grid, rounded to whole tiles.
    /// </summary>
    public Vector3 tilePositionFloor
    {
        get
        {
            CheckCast();
            return m_tilePositionFloor;
        }
    }

    public Vector3 roundedPositionFloor
    {
        get
        {
            CheckCast();
            return m_roundedPositionFloor;
        }
    }

    public Floor floor
    {
        get
        {
            CheckCast();
            return m_floor;
        }
    }

    public int subFloorIndex
    {
        get
        {
            CheckCast();
            return m_subFloorIndex;
        }
    }
    #endregion

    #region Private
    /// <summary>
    /// Whether a new raycast has been performed this frame.
    /// </summary>
    private bool m_castThisFrame;

    private RaycastHit m_castInfo;
    private Vector3 m_roundedPosition;
    private GameObject m_hitObject;

    private RaycastHit m_castInfoFloor;
    private Vector3 m_tilePositionFloor;
    private Vector3 m_roundedPositionFloor;
    private Floor m_floor;
    private int m_subFloorIndex;
    #endregion
    #endregion

    #region Unity
    private void Start()
    {
        m_pointer.SetActive(false);
    }

    private void Update()
    {
        m_castThisFrame = false;
        if (m_pointer.activeSelf)
            UpdatePointerPosition();
    }
    #endregion

    #region Public
    /// <summary>
    /// Activates/deactivates pointer object.
    /// </summary>
    /// <param name="a_value"></param>
    public void SetActivePointer(bool a_value)
    {
        m_pointer.SetActive(a_value);
    }
    #endregion

    #region Private
    /// <summary>
    /// Checks if the manager has already cast the mouse to world position this frame. If not, executes the cast.
    /// </summary>
    private void CheckCast()
    {
        if (!m_castThisFrame) { CastMouseRay(); }
    }

    /// <summary>
    /// Casts a ray from the main camera position that goes through the mouse to world position.
    /// </summary>
    private void CastMouseRay()
    {
        m_castThisFrame = true;

        // Regular cast
        Physics.Raycast(camera.transform.position, mouseDir, out m_castInfo);
        m_roundedPosition = new Vector3(Mathf.Round(m_castInfo.point.x), Mathf.Round(m_castInfo.point.y), Mathf.Round(m_castInfo.point.z));
        if (m_castInfo.collider != null) { m_hitObject = m_castInfo.collider.gameObject; }
        else { m_hitObject = null; }

        // Floor cast
        Physics.Raycast(camera.transform.position, mouseDir, out m_castInfoFloor, float.PositiveInfinity, 1 << 8);
        m_tilePositionFloor = new Vector3(Mathf.Floor(m_castInfoFloor.point.x), Mathf.Floor(m_castInfoFloor.point.y), Mathf.Floor(m_castInfoFloor.point.z));
        m_roundedPositionFloor = new Vector3(Mathf.Round(m_castInfoFloor.point.x), Mathf.Round(m_castInfoFloor.point.y), Mathf.Round(m_castInfoFloor.point.z));
        if (m_castInfoFloor.collider != null)
        {
            FloorComponent floorComp = m_castInfoFloor.collider.GetComponent<FloorComponent>();
            if (floorComp != null)
            {
                m_floor = floorComp.floor;
                m_subFloorIndex = floorComp.subFloorIndex;
            }
        }
    }

    private void UpdatePointerPosition()
    {
        // TODO: Pointer should extend past grid
        if (MathExtensions.IsBetween(s_roundedPositionFloor.x, 0, LotManager.width) &&
            MathExtensions.IsBetween(s_roundedPositionFloor.z, 0, LotManager.length))
        {
            m_pointer.transform.position = s_roundedPositionFloor;
        }
    }
    #endregion
}
