﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "Furniture Data", menuName = "Data/Furniture", order = 1)]
public class FurnitureData : ScriptableObject {

    public int id { get { return m_id; } }
    public string displayName { get { return m_displayName; } }
    public GameObject prefab { get { return m_prefab; } }

    [SerializeField]
    private int m_id;

    [SerializeField]
    [FormerlySerializedAs("m_name")]
    private string m_displayName;

    [SerializeField]
    [TextArea]
    private string m_description;

    [SerializeField]
    private GameObject m_prefab;
}

public static class FurnitureDataHolder
{
    // TODO: make read-only
    public static Dictionary<int, FurnitureData> data;

    // TODO: make UI with
    static FurnitureDataHolder()
    {
        data = new Dictionary<int, FurnitureData>();
        FurnitureData[] tempData = Resources.LoadAll<FurnitureData>("Prefabs/Buy Mode/Data");
        foreach (FurnitureData dataEntry in tempData)
            data.Add(dataEntry.id, dataEntry);
    }
}