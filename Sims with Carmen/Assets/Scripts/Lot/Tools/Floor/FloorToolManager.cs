﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Floors will be in datastructures that containing the links to other, connected floors and all the data that is on the floor.

public class FloorToolManager : MonoBehaviour {

    public MeshFilter floorMesh;

    private int m_mouseTile;

    private void Start()
    {
        m_mouseTile = 2;
    }

    void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            //SetToMouseTile();
        }
	}
    
    private void SetToMouseTile()
    {
        Debug.Log(MouseManager.s_tilePositionFloor);
        Vector3 tilePos = MouseManager.s_tilePositionFloor;
        SetTile((int)(tilePos.x + tilePos.z * LotManager.width), m_mouseTile);
    }

    public void SetTile(int a_tileIndex, int a_textureIndex)
    {
        int[] triangles = FloorThings.TrianglesForTile(a_tileIndex);

        Vector2[] tileUvs = uvsForTile(a_textureIndex);

        Vector2[] uvs = floorMesh.mesh.uv;

        uvs[triangles[0]] = tileUvs[0];
        uvs[triangles[5]] = tileUvs[1];
        uvs[triangles[1]] = tileUvs[2];
        uvs[triangles[2]] = tileUvs[3];

        floorMesh.mesh.uv = uvs;
    }

    const int c_tilesInRow = 2;

    public static Vector2[] uvsForTile(int a_tileIndex)
    {
        float rate = 1f / c_tilesInRow;
        int tileX = a_tileIndex % c_tilesInRow;
        int tileY = a_tileIndex / c_tilesInRow;

        Vector2[] result = new Vector2[4];
        result[0] = new Vector2(tileX * rate, 1 - tileY * rate);
        result[1] = new Vector2((tileX + 1) * rate, 1 - tileY * rate);
        result[2] = new Vector2(tileX * rate, 1 - (tileY + 1) * rate);
        result[3] = new Vector2((tileX + 1) * rate, 1 - (tileY + 1) * rate);
        return result;
    }
}
