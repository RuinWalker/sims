﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FloorComponent : MonoBehaviour
{
    public Floor floor { get; internal set; }
    public int subFloorIndex { get; internal set; }
}

// TODO: All created objects need to be sorted in parent objects.
public class Floor {

    #region Internal Classes
    #region Saving
    public class FloorHeader
    {
        public int index;
        public float[] subfloorsHeight;
        public List<List<bool>> horizontalWallTiles;
        public List<List<bool>> verticalWallTiles;
        public List<FurnitureHeader> furniture;

        /*
        To save per floor:
        set tiles
        objects on floor
        */

        public static FloorHeader ToHeader(Floor a_floor)
        {
            FloorHeader result = new FloorHeader();
            // Floor data
            result.index = a_floor.index;
            result.subfloorsHeight = new float[a_floor.m_floorObjects.Count];
            for (int i = 0; i < a_floor.m_floorObjects.Count; i++)
                result.subfloorsHeight[i] = a_floor.m_floorObjects[i].transform.position.y;
            // Walls
            result.horizontalWallTiles = a_floor.m_horizontalWallTiles;
            result.verticalWallTiles = a_floor.m_verticalWallTiles;
            // Furniture

            result.furniture = new List<FurnitureHeader>();
            foreach (KeyValuePair<int, GameObject> pair in a_floor.m_furniture)
            {
                result.furniture.Add(new FurnitureHeader(pair));
            }
            return result;
        }
    }

    public class FoundationHeader
    {
        public List<int> subfloorIndices;
        public List<List<int>> foundationTiles;

        public static FoundationHeader ToHeader(Dictionary<int, List<int>> a_foundationTiles)
        {
            FoundationHeader result = new FoundationHeader();
            result.subfloorIndices = a_foundationTiles.Keys.ToList();
            result.foundationTiles = new List<List<int>>();
            for(int i = 0; i < result.subfloorIndices.Count; i++)
            {
                result.foundationTiles.Add(
                    a_foundationTiles[result.subfloorIndices[i]].ToList()
                );
            }
            return result;
        }
    }

    public class FurnitureHeader
    {
        public int instanceId;
        public int objectId;
        public float[] position;
        public float[] rotation;

        public FurnitureHeader()
        {
            position = new float[3];
            rotation = new float[3];
        }

        public FurnitureHeader(KeyValuePair<int, GameObject> a_dataPair) : this()
        {
            instanceId = a_dataPair.Key;

            objectId = a_dataPair.Value.GetComponent<Furniture>().data.id;

            Vector3 posToSave = a_dataPair.Value.transform.position;
            position[0] = posToSave.x;
            position[1] = posToSave.y;
            position[2] = posToSave.z;

            Vector3 rosToSave = a_dataPair.Value.transform.eulerAngles;
            rotation[0] = rosToSave.x;
            rotation[1] = rosToSave.y;
            rotation[2] = rosToSave.z;
        }
    }
    #endregion

    /// <summary>
    /// Container class for the Depth First Search for walls.
    /// </summary>
    public class WallsDepthFirstContainer
    {
        /// <summary>
        /// Container class for a visited wall tile.
        /// </summary>
        public class Visited
        {
            /// <summary>
            /// Index of the wall tile.
            /// </summary>
            public int index { get; private set; }
            /// <summary>
            /// Direction of the wall tile.
            /// </summary>
            public WallTool.WallDirection direction { get; private set; }

            /// <summary>
            /// Whether the store tile is the same as the supplied stored tile.
            /// </summary>
            /// <param name="a_other"></param>
            /// <returns></returns>
            public bool IsSame(Visited a_other)
            {
                return IsSame(a_other.index, a_other.direction);
            }

            /// <summary>
            /// Whether the store tile is the same as the supplied stored tile.
            /// </summary>
            /// <param name="a_index"></param>
            /// <param name="a_direction"></param>
            /// <returns></returns>
            public bool IsSame(int a_index, WallTool.WallDirection a_direction)
            {
                if (index != a_index) return false;
                switch (direction)
                {
                    case WallTool.WallDirection.left:
                    case WallTool.WallDirection.right:
                        return a_direction == WallTool.WallDirection.left || a_direction == WallTool.WallDirection.right;
                    case WallTool.WallDirection.forward:
                    case WallTool.WallDirection.back:
                        return a_direction == WallTool.WallDirection.forward || a_direction == WallTool.WallDirection.back;
                    default:
                        return false;
                }
            }

            public Visited(int a_index, WallTool.WallDirection a_direction)
            {
                index = a_index;
                direction = a_direction;
            }
        }

        /// <summary>
        /// Reference to the horizontal wall tiles on the current floor.
        /// </summary>
        public List<bool> horzWallTiles { get; private set; }
        /// <summary>
        /// Reference to the vertical wall tiles on the current floor.
        /// </summary>
        public List<bool> vertWallTiles { get; private set; }
        /// <summary>
        /// The visited horizontal wall tiles.
        /// </summary>
        public bool[] visitedHorzTiles { get; private set; }
        /// <summary>
        /// The visited vertical wall tiles.
        /// </summary>
        public bool[] visitedVertTiles { get; private set; }
        /// <summary>
        /// The path of currently visited tiles.
        /// </summary>
        public List<Visited> stack { get; private set; }

        public WallsDepthFirstContainer(List<bool> a_horzWallTiles, List<bool> a_vertWallTiles)
        {
            horzWallTiles = a_horzWallTiles;
            vertWallTiles = a_vertWallTiles;
            visitedHorzTiles = new bool[LotManager.width * (LotManager.length + 1)];
            visitedVertTiles = new bool[(LotManager.width + 1) * LotManager.length];
            stack = new List<Visited>();
        }
    }
    #endregion

    #region Variables
    public static GameObject s_floorPrefab;
    public static GameObject s_wallPrefab;

    /// <summary>
    /// The floor below this floor
    /// </summary>
    public Floor lowerFloor { get; private set; }
    /// <summary>
    /// The floor above this floor
    /// </summary>
    public Floor higherFloor { get; private set; }

    /// <summary>
    /// Is the ground floor?
    /// </summary>
    public bool isGroundFloor { get; private set; }
    /// <summary>
    /// Index of this floor
    /// </summary>
    public int index { get; private set; }

    /// <summary>
    /// The game objects for all sub floors
    /// </summary>
    public List<GameObject> m_floorObjects;
    /// <summary>
    /// The mesh filters for all sub floors
    /// </summary>
    public List<MeshFilter> m_floorFilters;
    /// <summary>
    /// The colliders for all sub floor
    /// </summary>
    public List<MeshCollider> m_floorColliders;

    /// <summary>
    /// The objects for all wall collections
    /// </summary>
    public List<GameObject> m_wallObjects;
    /// <summary>
    /// Does this tile on the sub floor have a horizontal wall?
    /// </summary>
    public List<List<bool>> m_horizontalWallTiles;
    /// <summary>
    /// Does this tile on the sub floor have a vertical wall?
    /// </summary>
    public List<List<bool>> m_verticalWallTiles;
    /// <summary>
    /// The wall info for this horizontal wall tile
    /// </summary>
    public List<Dictionary<int, WallMeshInfo>> m_wallInfosHorz;
    /// <summary>
    /// The wall info for this vertical wall tile
    /// </summary>
    public List<Dictionary<int, WallMeshInfo>> m_wallInfosVert;

    /// <summary>
    /// The furniture on this floor
    /// </summary>
    public Dictionary<int, GameObject> m_furniture;
    #endregion

    #region Methods

    #region Constructors
    private void ResourcesCheck()
    {
        if (s_floorPrefab == null) { s_floorPrefab = Resources.Load<GameObject>("Prefabs/Second Floor"); }
        if (s_wallPrefab == null) { s_wallPrefab = Resources.Load<GameObject>("Prefabs/Wall"); }
    }

    private void InitializeMembers()
    {
        m_floorObjects = new List<GameObject>();
        m_floorFilters = new List<MeshFilter>();
        m_floorColliders = new List<MeshCollider>();
        m_wallObjects = new List<GameObject>();
        m_horizontalWallTiles = new List<List<bool>>();
        m_verticalWallTiles = new List<List<bool>>();
        m_wallInfosHorz = new List<Dictionary<int, WallMeshInfo>>();
        m_wallInfosVert = new List<Dictionary<int, WallMeshInfo>>();
        m_furniture = new Dictionary<int, GameObject>();
    }
    /// <summary>
    /// General constructor
    /// </summary>
    protected Floor()
    {
        ResourcesCheck();
        InitializeMembers();   
    }

    /// <summary>
    /// Constructor for ground floor
    /// </summary>
    /// <param name="a_floor"></param>
    public Floor(GameObject a_floor) : this()
    {
        int index = m_floorObjects.Count;
        // Floor
        m_floorObjects.Add(a_floor);
        m_floorFilters.Add(a_floor.GetComponent<MeshFilter>());
        m_floorColliders.Add(a_floor.GetComponent<MeshCollider>());

        // Walls
        m_wallObjects.Add(UnityEngine.Object.Instantiate(s_wallPrefab, new Vector3(0, 0, 0), Quaternion.identity));
        m_horizontalWallTiles.Add(new List<bool>());
        m_verticalWallTiles.Add(new List<bool>());
        for (int i = 0; i < TerrainTest.width * (TerrainTest.length + 1); i++) m_horizontalWallTiles[index].Add(false); // + 1 row
        for (int i = 0; i < (TerrainTest.width + 1) * TerrainTest.length; i++) m_verticalWallTiles[index].Add(false); // + 1 on each row
        m_wallInfosHorz.Add(new Dictionary<int, WallMeshInfo>());
        m_wallInfosVert.Add(new Dictionary<int, WallMeshInfo>());

        isGroundFloor = true;
        index = 0;
    }

    /// <summary>
    /// Constructor for additional floors
    /// </summary>
    /// <param name="a_floor"></param>
    public Floor(Floor a_parent, bool a_isHigherFloor) : this()
    {
        if (a_isHigherFloor)
        {
            a_parent.higherFloor = this;
            lowerFloor = a_parent;
            index = a_parent.index + 1;
        }
        else
        {
            a_parent.lowerFloor = this;
            higherFloor = a_parent;
            index = a_parent.index - 1;
        }
    }

    public Floor(FloorHeader a_header, Floor a_lowerFloor) : this()
    {
        if (a_lowerFloor != null)
        {
            lowerFloor = a_lowerFloor;
            lowerFloor.higherFloor = this;
        }
        index = a_header.index;
        for (int i = 0; i < a_header.subfloorsHeight.Length; i++)
        {
            AddFloor(a_header.subfloorsHeight[i]);
            m_horizontalWallTiles[i] = a_header.horizontalWallTiles[i];
            for (int j = 0; j < m_horizontalWallTiles[i].Count; j++)
            {
                if (m_horizontalWallTiles[i][j])
                    WallTool.AddWall(j, WallTool.WallDirection.left, this, i);
            }
            m_verticalWallTiles[i] = a_header.verticalWallTiles[i];
            for (int j = 0; j < m_verticalWallTiles[i].Count; j++)
            {
                if (m_verticalWallTiles[i][j])
                    WallTool.AddWall(j, WallTool.WallDirection.forward, this, i);
            }
        }
        // Furniture
        if (a_header.furniture != null)
        {
            PlacementController placementController = PlacementController.Instance;
            foreach (FurnitureHeader furnitureHeader in a_header.furniture)
            {
                Vector3 position = new Vector3(furnitureHeader.position[0], furnitureHeader.position[1], furnitureHeader.position[2]);
                Vector3 rotation = new Vector3(furnitureHeader.rotation[0], furnitureHeader.rotation[1], furnitureHeader.rotation[2]);
                placementController.PlaceObject(furnitureHeader.objectId, this, position, Quaternion.Euler(rotation), furnitureHeader.instanceId);
            }
        }
        // TODO: available tiles
    }
    #endregion

    public int IndexOf(float a_height)
    {
        for (int i = 0; i < m_floorObjects.Count; i++)
        {
            if (m_floorObjects[i].transform.position.y == a_height) { return i; }
        }
        return -1;
    }

    public int AddFloor(float a_height)
    {
        int subfloorIndex = m_floorObjects.Count;

        // < FLOOR >
        // Create the ground plane for the new floor
        GameObject newObj = UnityEngine.Object.Instantiate(s_floorPrefab, new Vector3(0, a_height, 0), Quaternion.identity);
        newObj.name = "Floor " + index + " - Subfloor " + subfloorIndex;  
        // Set mesh
        MeshFilter filter = newObj.GetComponent<MeshFilter>();
        filter.mesh = FloorThings.MakeStraightFloorNoTris();
        MeshCollider collider = newObj.GetComponent<MeshCollider>();
        collider.sharedMesh = filter.mesh;
        // Set FloorComponent values
        FloorComponent floorComp = newObj.GetComponent<FloorComponent>();
        floorComp.floor = this;
        floorComp.subFloorIndex = subfloorIndex;
        // Deactivate since new floors are hidden by default (TODO: needs to check if current floor is hidden or not)
        newObj.SetActive(false);
        // Add everything to the arrays
        m_floorObjects.Add(newObj);
        m_floorFilters.Add(filter);
        m_floorColliders.Add(collider);

        // < WALL >
        // Initialize wall tiles
        m_horizontalWallTiles.Add(new List<bool>());
        m_verticalWallTiles.Add(new List<bool>());
        for (int i = 0; i < TerrainTest.width * (TerrainTest.length + 1); i++) m_horizontalWallTiles[subfloorIndex].Add(false); // + 1 row
        for (int i = 0; i < (TerrainTest.width + 1) * TerrainTest.length; i++) m_verticalWallTiles[subfloorIndex].Add(false); // + 1 on each row

        m_wallObjects.Add(UnityEngine.Object.Instantiate(s_wallPrefab, new Vector3(0, a_height, 0), Quaternion.identity));
        m_wallObjects[subfloorIndex].SetActive(false);

        m_wallInfosHorz.Add(new Dictionary<int, WallMeshInfo>());
        m_wallInfosVert.Add(new Dictionary<int, WallMeshInfo>());

        return m_floorObjects.Count - 1;
    }

    #region Available Tiles Wall & Depth First
    public void RefreshAvailableTilesWalls(int a_subFloorIndex)
    {
        List<bool> availableTiles = new List<bool>();
        for (int i = 0; i < TerrainTest.width * TerrainTest.length; i++) { availableTiles.Add(false); }

        // Horizontal tiles
        for (int i = 0; i < m_horizontalWallTiles[a_subFloorIndex].Count; i++)
        {
            if (m_horizontalWallTiles[a_subFloorIndex][i])
            {
                int x = i % TerrainTest.width;
                int y1 = i / TerrainTest.width;
                int y2 = y1 - 1;
                bool xBetween = MathExtensions.IsBetween(x, 0, TerrainTest.width - 1);
                if (MathExtensions.IsBetween(y1, 0, TerrainTest.length - 1) && xBetween) { availableTiles[y1 * TerrainTest.width + x] = true; }
                if (MathExtensions.IsBetween(y2, 0, TerrainTest.length - 1) && xBetween) { availableTiles[y2 * TerrainTest.width + x] = true; }
            }
        }
        // Vertical tiles
        for (int i = 0; i < m_verticalWallTiles[a_subFloorIndex].Count; i++)
        {
            if (m_verticalWallTiles[a_subFloorIndex][i])
            {
                int x1 = i % (TerrainTest.width + 1);
                int x2 = x1 - 1;
                int y = i / (TerrainTest.width + 1);
                bool yBetween = MathExtensions.IsBetween(y, 0, TerrainTest.length - 1);
                if (MathExtensions.IsBetween(x1, 0, TerrainTest.width - 1) && yBetween) { availableTiles[y * TerrainTest.width + x1] = true; }
                if (MathExtensions.IsBetween(x2, 0, TerrainTest.width - 1) && yBetween) { availableTiles[y * TerrainTest.width + x2] = true; }
            }
        }

        // DEPTH FIRST SEARCH
        {
            WallsDepthFirstContainer container = new WallsDepthFirstContainer(m_horizontalWallTiles[a_subFloorIndex], m_verticalWallTiles[a_subFloorIndex]);
            for (int i = 0; i < container.visitedHorzTiles.Length; i++)
            {
                if (!container.visitedHorzTiles[i] && container.horzWallTiles[i])
                    DepthFirst(i, WallTool.WallDirection.right, container, availableTiles);
            }
        }
        List<int> finalTriangles = new List<int>();
        for (int i = 0; i < availableTiles.Count; i++) { if (availableTiles[i]) { finalTriangles.AddRange(FloorThings.TrianglesForTile(i)); } }

        // Check if created second floor before. If not, create it.
        if (higherFloor == null) { Floor newFloor = new Floor(this, true); }
        float floorHeight = m_floorObjects[a_subFloorIndex].transform.position.y + WallTool.c_wallHeight;
        int index = higherFloor.IndexOf(floorHeight);
        if (index < 0) { index = higherFloor.AddFloor(floorHeight); }

        higherFloor.m_floorFilters[index].mesh.triangles = finalTriangles.ToArray();
        FloorThings.Recalculate(higherFloor.m_floorFilters[index].mesh, higherFloor.m_floorColliders[index]);
    }

    private void DepthFirst(int a_index, WallTool.WallDirection a_direction, WallsDepthFirstContainer a_visited, List<bool> a_availableTiles)
    {
        // Add tile to list
        WallsDepthFirstContainer.Visited tile = new WallsDepthFirstContainer.Visited(a_index, a_direction);
        a_visited.stack.Add(tile);

        // DepthFirst adjacent tile
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                {
                    Vector2Int a_pos = WallTool.ToPosition(a_index, a_direction);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.left, a_direction))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.left, a_direction), a_direction, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.right, a_direction))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.right, a_direction), a_direction, a_visited, a_availableTiles);

                    if (WallTool.IsValidPos(a_pos, WallTool.WallDirection.forward))
                        CheckTile(WallTool.ToIndex(a_pos, WallTool.WallDirection.forward), WallTool.WallDirection.forward, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.right, WallTool.WallDirection.forward))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.right, WallTool.WallDirection.forward), WallTool.WallDirection.forward, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.down, WallTool.WallDirection.back))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.down, WallTool.WallDirection.back), WallTool.WallDirection.back, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + new Vector2Int(1, -1), WallTool.WallDirection.back))
                        CheckTile(WallTool.ToIndex(a_pos + new Vector2Int(1, -1), WallTool.WallDirection.back), WallTool.WallDirection.back, a_visited, a_availableTiles);

                    break;
                }
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                {
                    Vector2Int a_pos = WallTool.ToPosition(a_index, a_direction);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.down, a_direction))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.down, a_direction), a_direction, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.up, a_direction))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.up, a_direction), a_direction, a_visited, a_availableTiles);

                    if (WallTool.IsValidPos(a_pos, WallTool.WallDirection.left))
                        CheckTile(WallTool.ToIndex(a_pos, WallTool.WallDirection.left), WallTool.WallDirection.left, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.left, WallTool.WallDirection.left))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.left, WallTool.WallDirection.left), WallTool.WallDirection.left, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + Vector2Int.up, WallTool.WallDirection.left))
                        CheckTile(WallTool.ToIndex(a_pos + Vector2Int.up, WallTool.WallDirection.left), WallTool.WallDirection.left, a_visited, a_availableTiles);
                    if (WallTool.IsValidPos(a_pos + new Vector2Int(-1, 1), WallTool.WallDirection.left))
                        CheckTile(WallTool.ToIndex(a_pos + new Vector2Int(-1, 1), WallTool.WallDirection.left), WallTool.WallDirection.left, a_visited, a_availableTiles);

                    break;

                }
        }

        // Remove this from list
        a_visited.stack.RemoveAt(a_visited.stack.Count - 1);
    }

    private void CheckTile(int a_index, WallTool.WallDirection a_direction, WallsDepthFirstContainer a_visited, List<bool> a_availableTiles)
    {
        List<bool> wallArray = new List<bool>();
        bool[] visitedArray = new bool[0];
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                wallArray = a_visited.horzWallTiles;
                visitedArray = a_visited.visitedHorzTiles;
                break;
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                wallArray = a_visited.vertWallTiles;
                visitedArray = a_visited.visitedVertTiles;
                break;

            case WallTool.WallDirection.none:
                return;
        }

        // Check if trying to check the previous tile
        // Do -2 instead of -1 because the current tile has already been added to the stack.
        if (a_visited.stack.Count >= 2 && a_visited.stack[a_visited.stack.Count - 2].IsSame(a_index, a_direction))
            return;

        // If has a wall here
        if (wallArray[a_index])
        {
            // Check if already visited
            if (a_visited.stack.Any(x => x.IsSame(a_index, a_direction)))
            {
                // Make loop
                for (int i = a_visited.stack.Count - 1; i >= 0; i--)
                {
                    if (a_visited.stack[i].IsSame(a_index, a_direction)) { MakeWallLoop(i, a_visited, a_availableTiles); }
                }
            }
            else
            {
                visitedArray[a_index] = true;
                DepthFirst(a_index, a_direction, a_visited, a_availableTiles);
            }
        }
    }

    private void MakeWallLoop(int a_startIndex, WallsDepthFirstContainer a_visited, List<bool> a_availableTiles)
    {
        byte[] wallsPerTiles = new byte[a_availableTiles.Count];

        for (int i = a_startIndex; i < a_visited.stack.Count; i++)
        {
            WallsDepthFirstContainer.Visited current = a_visited.stack[i];
            switch (current.direction)
            {
                case WallTool.WallDirection.left:
                case WallTool.WallDirection.right:
                    int x = current.index % TerrainTest.width;
                    int y1 = current.index / TerrainTest.width;
                    int y2 = y1 - 1;
                    bool xBetween = MathExtensions.IsBetween(x, 0, TerrainTest.width - 1);
                    if (MathExtensions.IsBetween(y1, 0, TerrainTest.length - 1) && xBetween)
                    {
                        int index = WallTool.ToIndex(x, y1, current.direction); //y1 * TerrainTest.width + x;
                        a_availableTiles[index] = true;
                        wallsPerTiles[index] |= 1;
                    }
                    if (MathExtensions.IsBetween(y2, 0, TerrainTest.length - 1) && xBetween)
                    {
                        int index = WallTool.ToIndex(x, y2, current.direction); //y2 * TerrainTest.width + x;
                        a_availableTiles[index] = true;
                        wallsPerTiles[index] |= (1 << 1);
                    }
                    break;
                case WallTool.WallDirection.forward:
                case WallTool.WallDirection.back:
                    int x1 = current.index % (TerrainTest.width + 1);
                    int x2 = x1 - 1;
                    int y = current.index / (TerrainTest.width + 1);
                    bool yBetween = MathExtensions.IsBetween(y, 0, TerrainTest.length - 1);
                    if (MathExtensions.IsBetween(x1, 0, TerrainTest.width - 1) && yBetween)
                    {
                        int index = WallTool.ToIndex(x1, y, current.direction);//y * TerrainTest.width + x1;
                        a_availableTiles[index] = true;
                        wallsPerTiles[index] |= (1 << 2);
                    }
                    if (MathExtensions.IsBetween(x2, 0, TerrainTest.width - 1) && yBetween)
                    {
                        int index = WallTool.ToIndex(x2, y, current.direction);//y * TerrainTest.width + x2;
                        a_availableTiles[index] = true;
                        wallsPerTiles[index] |= (1 << 3);
                    }
                    break;
            }
        }

        bool inRoom = false;
        for (int x = 0; x < LotManager.width; x++)
        {
            for (int y = 0; y < LotManager.length; y++)
            {
                int index = y * TerrainTest.width + x;
                if (inRoom)
                    a_availableTiles[index] = true;
                // 1 is wall below
                // 1 << 1 is wall above
                // 1 << 2 is wall to the left
                // 1 << 3 is wall to the right
                if ((wallsPerTiles[index] & (1 << 1)) > 0)
                    inRoom = !inRoom;
            }
        }
    }

    public void RefreshAvailableFoundationTiles(int[] a_addedTiles, float a_height)
    {
        // Check if created second floor before. If not, create it.
        if (higherFloor == null) { Floor newFloor = new Floor(this, true); }
        float floorHeight = m_floorObjects[0].transform.position.y + a_height;
        int index = higherFloor.IndexOf(FoundationTool.c_foundationHeight);
        if (index < 0) { index = higherFloor.AddFloor(floorHeight); }

        if (!LotManager.Instance.foundationTiles.ContainsKey(index))
            LotManager.Instance.foundationTiles.Add(index, new List<int>());
        LotManager.Instance.foundationTiles[index].AddRange(a_addedTiles);
        List<int> finalTriangles = new List<int>();
        List<int> foundationTiles = LotManager.Instance.foundationTiles[index];
        for (int i = 0; i < foundationTiles.Count; i++)
        {
            finalTriangles.AddRange(FloorThings.TrianglesForTile(foundationTiles[i]));
        }
        higherFloor.m_floorFilters[index].mesh.triangles = finalTriangles.ToArray();
        FloorThings.Recalculate(higherFloor.m_floorFilters[index].mesh, higherFloor.m_floorColliders[index]);
    }

    #endregion
    #endregion
}
