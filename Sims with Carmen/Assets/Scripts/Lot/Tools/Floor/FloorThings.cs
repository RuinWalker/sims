﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FloorThings {

    public static int width { get { return LotManager.width; } }
    public static int length { get { return LotManager.length; } }

    public static int[] TrianglesForTile(int tileIndex)
    {
        return TrianglesForTile(tileIndex % width, tileIndex / width);
    }

    public static int[] TrianglesForTile(int tileX, int tileY)
    {
        int[] triangles = new int[6];

        int firstVertIndex = (tileX + tileY * width) * 4;
        triangles[0] = firstVertIndex;
        triangles[1] = firstVertIndex + 2;
        triangles[2] = firstVertIndex + 3;
        triangles[3] = firstVertIndex;
        triangles[4] = firstVertIndex + 3;
        triangles[5] = firstVertIndex + 1;

        return triangles;
    }

    public static int[] VerticesForTile(int tileX, int tileY)
    {
        List<int> vertices = new List<int>(TrianglesForTile(tileX, tileY));

        // Left
        if (tileX - 1 >= 0)
        {
            int firstVertexIndex = (tileX - 1 + tileY * width) * 4;
            vertices.Add(firstVertexIndex + 1);
            vertices.Add(firstVertexIndex + 3);
        }
        // Right
        if (tileX + 1 < width)
        {
            int firstVertIndex2 = (tileX + 1 + tileY * width) * 4;
            vertices.Add(firstVertIndex2);
            vertices.Add(firstVertIndex2 + 2);
        }
        // Down
        if (tileY - 1 >= 0)
        {
            int firstVertIndex3 = (tileX + (tileY - 1) * width) * 4;
            vertices.Add(firstVertIndex3 + 2);
            vertices.Add(firstVertIndex3 + 3);
        }
        // Up
        if (tileY + 1 < length)
        {
            int firstVertIndex4 = (tileX + (tileY + 1) * width) * 4;
            vertices.Add(firstVertIndex4);
            vertices.Add(firstVertIndex4 + 1);
        }

        // Bottom Left
        if (tileX - 1 >= 0 && tileY - 1 >= 0)
        {
            int firstVertexIndex = (tileX - 1 + (tileY - 1) * width) * 4;
            vertices.Add(firstVertexIndex + 3);
        }

        // Bottom Right
        if (tileX + 1 < width && tileY - 1 >= 0)
        {
            int firstVertexIndex = (tileX + 1 + (tileY - 1) * width) * 4;
            vertices.Add(firstVertexIndex + 2);
        }

        // Top Left
        if (tileX - 1 >= 0 && tileY + 1 < length)
        {
            int firstVertexIndex = (tileX - 1 + (tileY + 1) * width) * 4;
            vertices.Add(firstVertexIndex + 1);
        }

        // Top Right
        if (tileX + 1 < width && tileY + 1 < length)
        {
            int firstVertexIndex = (tileX + 1 + (tileY + 1) * width) * 4;
            vertices.Add(firstVertexIndex);
        }

        return vertices.ToArray();
    }

    public static Mesh MakeStraightFloorNoTris()
    {
        int tileCount = width * length;
        Mesh mesh = new Mesh();
        mesh.name = "Floor";

        Vector3[] vertices = new Vector3[width * length * 4]; // 4 verts per tile
        Vector2[] uv = new Vector2[vertices.Length];

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < length; j++)
            {
                int firstVertIndex = (i + j * width) * 4;
                vertices[firstVertIndex] = new Vector3(i, 0, j);
                vertices[firstVertIndex + 1] = new Vector3(i + 1, 0, j);
                vertices[firstVertIndex + 2] = new Vector3(i, 0, j + 1);
                vertices[firstVertIndex + 3] = new Vector3(i + 1, 0, j + 1);

                Vector2[] uvs = FloorToolManager.uvsForTile(0);
                uv[firstVertIndex] = uvs[0];
                uv[firstVertIndex + 1] = uvs[1];
                uv[firstVertIndex + 2] = uvs[2];
                uv[firstVertIndex + 3] = uvs[3];
                /*
                int vertIndex = j * (width + 1) + i;
                vertices[vertIndex] = new Vector3(i, 0, j);
                uv[vertIndex] = new Vector2(i & 0x1, j & 0x1);
                */
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        return mesh;
    }

    public static Mesh MakeStraightFloor()
    {
        Mesh mesh = MakeStraightFloorNoTris();
        List<int> triangles = new List<int>();
        for (int j = 0; j < length; j++)
            for (int i = 0; i < width; i++)
            {
                triangles.AddRange(TrianglesForTile(i, j));
                /*
                int tileIndex = j * width + i; // Current tile
                int vertexIndex = j * (width + 1) + i; // Current tile
                int triangleIndex = tileIndex * 2; // Index first triangle of current tile
                int vertIndex = triangleIndex * 3; // Index first vert of first triangle of current tile in the triangle array

                triangles[vertIndex + 0] = vertexIndex;
                triangles[vertIndex + 1] = vertexIndex + (width + 1);
                triangles[vertIndex + 2] = vertexIndex + (width + 1) + 1;

                triangles[vertIndex + 3] = vertexIndex;
                triangles[vertIndex + 4] = vertexIndex + (width + 1) + 1;
                triangles[vertIndex + 5] = vertexIndex + 1;
                */
            }
        mesh.triangles = triangles.ToArray();
        return mesh;
    }

    public static void Recalculate(Mesh a_mesh)
    {
        a_mesh.RecalculateNormals();
        a_mesh.RecalculateBounds();
    }

    public static void Recalculate(Mesh a_mesh, MeshCollider a_collider)
    {
        Recalculate(a_mesh);
        // Recalculates the collider mesh.
        a_collider.sharedMesh = a_mesh;
        a_collider.enabled = false;
        a_collider.enabled = true;
    }
}
