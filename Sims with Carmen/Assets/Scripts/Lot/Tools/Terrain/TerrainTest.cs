﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.Networking;

// TODO: make TerrainData work properly with new mesh structure
// TODO: make terrain editing work properly with new mesh structure
public class TerrainData
{
    public float[] m_vertices;
    public float[] m_uvs;
    public int[] m_triangles;

    public Mesh ToMesh()
    {
        Mesh result = new Mesh();
        result.name = "Terrain";

        Vector3[] vertices = new Vector3[m_vertices.Length / 3];
        for (int i = 0; i < m_vertices.Length; i += 3)
            vertices[i / 3] = new Vector3(m_vertices[i], m_vertices[i + 1], m_vertices[i + 2]);
        result.vertices = vertices;
        if (m_uvs != null)
        {
            Vector2[] uvs = new Vector2[m_vertices.Length / 2];
            for (int i = 0; i < m_uvs.Length; i += 2)
                uvs[i / 2] = new Vector2(m_uvs[i], m_uvs[i + 1]);
            result.uv = uvs;
        }
        result.triangles = m_triangles;
        return result;
    }

    public string ToJson()
    {
        return JsonMapper.ToJson(this);
    }

    public static TerrainData FromJson(string a_json)
    {
        return JsonMapper.ToObject<TerrainData>(a_json);
    }

    public static TerrainData FromJson(JsonData a_jsonData)
    {
        return FromJson(a_jsonData.ToJson());
    }

    public static TerrainData FromMesh(Mesh a_mesh)
    {
        TerrainData result = new TerrainData();
        { 
            float[] vertices = new float[a_mesh.vertices.Length * 3];
            for (int i = 0; i < a_mesh.vertices.Length; i++)
            {
                Vector3 vertex = a_mesh.vertices[i];
                int index = i * 3;
                vertices[index] = vertex.x;
                vertices[index + 1] = vertex.y;
                vertices[index + 2] = vertex.z;
            }
            result.m_vertices = vertices;
        }
        {
            float[] uvs = new float[a_mesh.uv.Length * 2];
            for (int i = 0; i < a_mesh.uv.Length; i++)
            {
                Vector2 uv = a_mesh.uv[i];
                int index = i * 2;
                uvs[index] = uv.x;
                uvs[index + 1] = uv.y;
            }
            result.m_uvs = uvs;
        }
        result.m_triangles = a_mesh.triangles;
        return result;
    }
}

public class TerrainTest : MonoBehaviour {

    public enum Mode { level, raise, lower, smooth };

    public const int width = 10;
    public const int length = 10;

    public GameObject terrain { get { return LotManager.Instance.groundFloor.m_floorObjects[0]; } }
    public MeshFilter terrainFilter { get { return LotManager.Instance.groundFloor.m_floorFilters[0]; } }
    public MeshCollider terrainCollider { get { return LotManager.Instance.groundFloor.m_floorColliders[0]; } }

    private Mode m_activeMode;
    private bool m_levelling;
    private Vector3 m_levelStartPoint;

    // Use this for initialization
    void Start () {
        /*
        m_meshFilterComp = m_terrainObject.GetComponent<MeshFilter>();
        m_meshColliderComp = m_terrainObject.GetComponent<MeshCollider>();

        m_terrainMesh = FloorThings.MakeStraightFloor();
        m_meshFilterComp.mesh = m_terrainMesh;
        m_meshColliderComp.sharedMesh = m_terrainMesh;
        FloorThings.Recalculate(m_terrainMesh, m_meshColliderComp);
        */
        /*
        GenerateTerrain();
        */
        /*
        m_terrainMesh = TerrainData.FromJson("{\"m_vertices\":[0.0,1.0,0.0,1.0,0.0,0.0,2.0,0.0,0.0,3.0,0.0,0.0,4.0,0.0,0.0,5.0,0.0,0.0,6.0,1.0,0.0,7.0,0.0,0.0,8.0,0.0,0.0,9.0,1.0,0.0,10.0,0.0,0.0,0.0,0.0,1.0,1.0,0.0,1.0,2.0,0.0,1.0,3.0,0.0,1.0,4.0,0.0,1.0,5.0,0.0,1.0,6.0,0.0,1.0,7.0,0.0,1.0,8.0,0.0,1.0,9.0,0.0,1.0,10.0,0.0,1.0,0.0,0.0,2.0,1.0,0.0,2.0,2.0,0.0,2.0,3.0,0.0,2.0,4.0,0.0,2.0,5.0,0.0,2.0,6.0,0.0,2.0,7.0,0.0,2.0,8.0,0.0,2.0,9.0,1.0,2.0,10.0,0.0,2.0,0.0,0.0,3.0,1.0,0.0,3.0,2.0,0.0,3.0,3.0,0.0,3.0,4.0,0.0,3.0,5.0,0.0,3.0,6.0,0.0,3.0,7.0,1.0,3.0,8.0,0.0,3.0,9.0,0.0,3.0,10.0,0.0,3.0,0.0,0.0,4.0,1.0,0.0,4.0,2.0,0.0,4.0,3.0,0.0,4.0,4.0,0.0,4.0,5.0,0.0,4.0,6.0,0.0,4.0,7.0,0.0,4.0,8.0,0.0,4.0,9.0,0.0,4.0,10.0,0.0,4.0,0.0,0.0,5.0,1.0,0.0,5.0,2.0,0.0,5.0,3.0,0.0,5.0,4.0,0.0,5.0,5.0,0.0,5.0,6.0,0.0,5.0,7.0,0.0,5.0,8.0,0.0,5.0,9.0,0.0,5.0,10.0,0.0,5.0,0.0,0.0,6.0,1.0,0.0,6.0,2.0,0.0,6.0,3.0,0.0,6.0,4.0,0.0,6.0,5.0,0.0,6.0,6.0,0.0,6.0,7.0,0.0,6.0,8.0,0.0,6.0,9.0,0.0,6.0,10.0,0.0,6.0,0.0,1.0,7.0,1.0,0.0,7.0,2.0,0.0,7.0,3.0,0.0,7.0,4.0,0.0,7.0,5.0,0.0,7.0,6.0,0.0,7.0,7.0,1.0,7.0,8.0,0.0,7.0,9.0,0.0,7.0,10.0,0.0,7.0,0.0,0.0,8.0,1.0,0.0,8.0,2.0,0.0,8.0,3.0,0.0,8.0,4.0,0.0,8.0,5.0,0.0,8.0,6.0,0.0,8.0,7.0,0.0,8.0,8.0,0.0,8.0,9.0,0.0,8.0,10.0,0.0,8.0,0.0,0.0,9.0,1.0,0.0,9.0,2.0,1.0,9.0,3.0,0.0,9.0,4.0,0.0,9.0,5.0,0.0,9.0,6.0,0.0,9.0,7.0,1.0,9.0,8.0,0.0,9.0,9.0,2.0,9.0,10.0,0.0,9.0,0.0,0.0,10.0,1.0,0.0,10.0,2.0,0.0,10.0,3.0,0.0,10.0,4.0,0.0,10.0,5.0,0.0,10.0,6.0,0.0,10.0,7.0,0.0,10.0,8.0,0.0,10.0,9.0,0.0,10.0,10.0,0.0,10.0],\"m_uvs\":[0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0],\"m_triangles\":[0,11,12,0,12,1,1,12,13,1,13,2,2,13,14,2,14,3,3,14,15,3,15,4,4,15,16,4,16,5,5,16,17,5,17,6,6,17,18,6,18,7,7,18,19,7,19,8,8,19,20,8,20,9,9,20,21,9,21,10,11,22,23,11,23,12,12,23,24,12,24,13,13,24,25,13,25,14,14,25,26,14,26,15,15,26,27,15,27,16,16,27,28,16,28,17,17,28,29,17,29,18,18,29,30,18,30,19,19,30,31,19,31,20,20,31,32,20,32,21,22,33,34,22,34,23,23,34,35,23,35,24,24,35,36,24,36,25,25,36,37,25,37,26,26,37,38,26,38,27,27,38,39,27,39,28,28,39,40,28,40,29,29,40,41,29,41,30,30,41,42,30,42,31,31,42,43,31,43,32,33,44,45,33,45,34,34,45,46,34,46,35,35,46,47,35,47,36,36,47,48,36,48,37,37,48,49,37,49,38,38,49,50,38,50,39,39,50,51,39,51,40,40,51,52,40,52,41,41,52,53,41,53,42,42,53,54,42,54,43,44,55,56,44,56,45,45,56,57,45,57,46,46,57,58,46,58,47,47,58,59,47,59,48,48,59,60,48,60,49,49,60,61,49,61,50,50,61,62,50,62,51,51,62,63,51,63,52,52,63,64,52,64,53,53,64,65,53,65,54,55,66,67,55,67,56,56,67,68,56,68,57,57,68,69,57,69,58,58,69,70,58,70,59,59,70,71,59,71,60,60,71,72,60,72,61,61,72,73,61,73,62,62,73,74,62,74,63,63,74,75,63,75,64,64,75,76,64,76,65,66,77,78,66,78,67,67,78,79,67,79,68,68,79,80,68,80,69,69,80,81,69,81,70,70,81,82,70,82,71,71,82,83,71,83,72,72,83,84,72,84,73,73,84,85,73,85,74,74,85,86,74,86,75,75,86,87,75,87,76,77,88,89,77,89,78,78,89,90,78,90,79,79,90,91,79,91,80,80,91,92,80,92,81,81,92,93,81,93,82,82,93,94,82,94,83,83,94,95,83,95,84,84,95,96,84,96,85,85,96,97,85,97,86,86,97,98,86,98,87,88,99,100,88,100,89,89,100,101,89,101,90,90,101,102,90,102,91,91,102,103,91,103,92,92,103,104,92,104,93,93,104,105,93,105,94,94,105,106,94,106,95,95,106,107,95,107,96,96,107,108,96,108,97,97,108,109,97,109,98,99,110,111,99,111,100,100,111,112,100,112,101,101,112,113,101,113,102,102,113,114,102,114,103,103,114,115,103,115,104,104,115,116,104,116,105,105,116,117,105,117,106,106,117,118,106,118,107,107,118,119,107,119,108,108,119,120,108,120,109]}").ToMesh();
        m_terrainMesh.name = "Terrain";
        m_meshFilterComp.mesh = m_terrainMesh;
        m_meshColliderComp.sharedMesh = m_terrainMesh;
        FloorThings.Recalculate(m_terrainMesh, m_meshColliderComp);
        */
    }

    private void Update()
    {
        // TODO: custom cast
        if (MouseManager.s_castInfo.collider == LotManager.Instance.groundFloor.m_floorColliders[0])
        {
            Vector3 localPoint = WorldToLocal(MouseManager.s_castInfo.point);
            // TODO: put in handle input
            switch (m_activeMode)
            {
                case Mode.level:
                    if (Input.GetMouseButtonDown(1))
                    {
                        m_levelling = true;
                        m_levelStartPoint = ClosestVertexToLocal(localPoint);
                    }

                    if (Input.GetMouseButtonUp(1))
                    {
                        if (m_levelling)
                        {
                            ActionLevelTerrain(m_levelStartPoint, localPoint);
                            m_levelling = false;
                        }
                    }
                    break;
                case Mode.raise:
                    if (Input.GetMouseButtonDown(1))
                    {
                        if (Input.GetKey(KeyCode.LeftShift))
                            ActionLowerTerrain(localPoint);
                        else
                            ActionRaiseTerrain(localPoint);
                    }
                    break;
                case Mode.lower:
                    if (Input.GetMouseButtonDown(1))
                    {
                        if (Input.GetKey(KeyCode.LeftShift))
                            ActionRaiseTerrain(localPoint);
                        else
                            ActionLowerTerrain(localPoint);
                    }
                    break;
            }
        }

        // TESTING: switch mode
        if (Input.GetKeyDown(KeyCode.B))
        {
            // TODO: make function for this, function should cancel any active input
            if (m_activeMode == Mode.level)
                m_activeMode = Mode.raise;
            else
                m_activeMode = Mode.level;
            Debug.Log("Active mode: " + m_activeMode);
        }

        // TESTING: save terrain
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TerrainData data = TerrainData.FromMesh(terrainFilter.sharedMesh);
            string json = data.ToJson();
            Debug.Log(json);
        }
    }

    private Vector3 WorldToLocal(Vector3 a_point)
    {
        return terrain.transform.InverseTransformPoint(a_point);
    }

    private Vector3 ClosestVertexToLocal(Vector3 a_localPosition)
    {
        int tileX = (int)a_localPosition.x;
        int tileZ = (int)a_localPosition.z;
        // If invalid tile, return (0, 0, 0)
        if (tileX < 0 || tileX >= LotManager.width || tileZ < 0 || tileZ >= LotManager.length)
            return Vector3.zero;
        // Get vertices in tile
        int[] triangles = FloorThings.TrianglesForTile(tileX, tileZ); // TODO: can be optimised because this has 2 duplicate vertices
        Vector3[] vertices = terrainFilter.sharedMesh.vertices;
        // Get first vertex as base closest
        Vector3 closestVertex = vertices[triangles[0]];
        float shortestDistanceSqr = (closestVertex - a_localPosition).sqrMagnitude;
        // Test all vertices against first vertex to see if they are closer
        for (int i = 1; i < triangles.Length; i++)
        {
            float distanceSqr = (vertices[triangles[i]] - a_localPosition).sqrMagnitude;
            if (distanceSqr < shortestDistanceSqr)
            {
                closestVertex = vertices[triangles[i]];
                shortestDistanceSqr = distanceSqr;
            }
        }
        return closestVertex;
    }

    private int[] GetVertices(Vector3 a_closestPoint)
    {
        int[] closestTile = new int[]
        {
                (int)a_closestPoint.x,
                (int)a_closestPoint.y,
                (int)a_closestPoint.z
        };
        int[] closestVertex = new int[]
        {
                Mathf.RoundToInt(a_closestPoint.x) - closestTile[0],
                Mathf.RoundToInt(a_closestPoint.y) - closestTile[1],
                Mathf.RoundToInt(a_closestPoint.z) - closestTile[2]
        };

        MeshFilter terrainFilter = LotManager.Instance.groundFloor.m_floorFilters[0];

        // -1's should be ignored when using the ids
        int[] vertexIds = { -1, -1, -1, -1 };

        int cornerX = closestVertex[0] == 0 ? 1 : 0;
        int cornerZ = closestVertex[2] == 0 ? 1 : 0;

        int tileX = closestVertex[0] == 0 ? closestTile[0] - 1 : closestTile[0] + 1;
        int tileZ = closestVertex[2] == 0 ? closestTile[2] - 1 : closestTile[2] + 1;

        bool xInBounds = tileX >= 0 && tileX < LotManager.width;
        bool zInBounds = tileZ >= 0 && tileZ < LotManager.length;

        // Closest
        vertexIds[0] = (closestTile[2] * LotManager.width + closestTile[0]) * 4 + closestVertex[2] * 2 + closestVertex[0];
        // Opposite X
        if (xInBounds)
            vertexIds[1] = (closestTile[2] * LotManager.width + tileX) * 4 + closestVertex[2] * 2 + cornerX;
        // Opposite Z
        if (zInBounds)
            vertexIds[2] = (tileZ * LotManager.width + closestTile[0]) * 4 + cornerZ * 2 + closestVertex[0];
        // Opposite X & Z
        if (xInBounds && zInBounds)
            vertexIds[3] = (tileZ * LotManager.width + tileX) * 4 + cornerZ * 2 + cornerX; 

        return vertexIds;
    }

    public void ActionLevelTerrain(Vector3 a_localStartPoint, Vector3 a_localEndPoint)
    {
        if (NetworkClient.active)
            PlayerController.mine.CmdLevelTerrain(a_localStartPoint, a_localEndPoint);
        else
            LevelTerrain(a_localStartPoint, a_localEndPoint);
    }

    // TODO: if levelling rect too big, limit to available space
    public void LevelTerrain(Vector3 a_localStartPoint, Vector3 a_localEndPoint)
    {
        // Determine levelling rect
        int minX = (int)Mathf.Min(a_localStartPoint.x, a_localEndPoint.x);
        int maxX = (int)Mathf.Max(a_localStartPoint.x, a_localEndPoint.x);
        int minZ = (int)Mathf.Min(a_localStartPoint.z, a_localEndPoint.z);
        int maxZ = (int)Mathf.Max(a_localStartPoint.z, a_localEndPoint.z);

        MeshFilter terrainFilterCache = terrainFilter;
        Vector3[] vertices = terrainFilterCache.sharedMesh.vertices;
        // Level over rect
        for (int x = minX; x <= maxX; x++)
            for (int z = minZ; z <= maxZ; z++)
            {
                int[] triangles = FloorThings.VerticesForTile(x, z);
                foreach (int vertex in triangles)
                    vertices[vertex].y = a_localStartPoint.y;
            }
        // Update mesh
        terrainFilterCache.sharedMesh.vertices = vertices;
        FloorThings.Recalculate(terrainFilterCache.sharedMesh, terrainCollider);
    }

    public void ActionRaiseTerrain(Vector3 a_localPoint)
    {
        if (NetworkClient.active)
            PlayerController.mine.CmdRaiseTerrain(a_localPoint);
        else
            RaiseTerrain(a_localPoint);
    }

    public void ActionLowerTerrain(Vector3 a_localPoint)
    {
        if (NetworkClient.active)
            PlayerController.mine.CmdLowerTerrain(a_localPoint);
        else
            LowerTerrain(a_localPoint);
    }

    // TODO: strength should be variable based on user setting
    public void RaiseTerrain(Vector3 a_localPoint)
    {
        TransformTerrain(a_localPoint, 1);
    }

    public void LowerTerrain(Vector3 a_localPoint)
    {
        TransformTerrain(a_localPoint, -1);
    }

    // TODO: should be able to affect a radius
    private void TransformTerrain(Vector3 a_localPoint, float a_change)
    {
        MeshFilter terrainFilterCache = terrainFilter;
        Vector3[] vertices = terrainFilterCache.sharedMesh.vertices;
        // Transform all vertices on point
        int[] verticesToTransform = GetVertices(a_localPoint);
        foreach (int id in verticesToTransform)
            if (id >= 0)
                vertices[id].y += a_change;
        // Update mesh
        terrainFilterCache.sharedMesh.vertices = vertices;
        FloorThings.Recalculate(terrainFilterCache.sharedMesh, terrainCollider);
    }
    
}
