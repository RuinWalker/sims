﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoundationIndexHolder
{
    public int m_startVertexIndex;
    public int m_startTriangleIndex;
}

public class FoundationIndicesHolder : MonoBehaviour
{

    Dictionary<int, FoundationIndexHolder> m_indexHolders;

    public void Start()
    {
        m_indexHolders = new Dictionary<int, FoundationIndexHolder>();
    }

    public void RemoveIndexHolder(int a_index)
    {
        int startIndexVert = 0;
        int startIndexTri = 0;
        startIndexVert = m_indexHolders[a_index].m_startVertexIndex;
        startIndexTri = m_indexHolders[a_index].m_startTriangleIndex;
        m_indexHolders.Remove(a_index);
        foreach (FoundationIndexHolder holder in m_indexHolders.Values)
        {
            if (holder.m_startVertexIndex > startIndexVert)
            {
                holder.m_startVertexIndex -= 16;
            }
            if (holder.m_startTriangleIndex > startIndexTri)
            {
                holder.m_startTriangleIndex -= 24;
            }
        }
    }

    public void AddIndexHolder(FoundationIndexHolder a_holder, int a_index)
    {
        m_indexHolders.Add(a_index, a_holder);
    }
}
