﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

// TODO: unified pointer class

/*
TODO
toggle foundation walls based on surrounding foundation tiles
increase thickness of the foundation model to be an actual wall
place tiles on foundation

while placing, shouldn't show actual foundation but with green/red material
foundation should always be on the groundfloor
foundation should automatically bring you to higher terrain
foundation conditions for not placing
*/

public class FoundationTool : MonoBehaviour {

    /*
    When active, create a square
    All tiles marked as foundation tiles and added to mesh/available tiles
    */
    #region Variables
    public enum Mode { add, remove };

    public const float c_foundationHeight = 0.75f;

    bool[] m_foundationTiles;
    private bool m_currentlyPlacing;

    private Mode m_currentMode = Mode.add;

    private int[] m_placedFoundation;

    private int[] m_previousTiles;
    Dictionary<int, FoundationMeshInfo> m_foundationInfos;
    public GameObject m_foundationObject;

    private Vector3 m_startPosition;

    private MeshFilter m_filter;

    private Mesh m_mesh;
    #endregion

    #region Methods
    #region Unity
    // Use this for initialization
    void Awake () {
        m_foundationTiles = new bool[LotManager.width * LotManager.length];
        m_foundationInfos = new Dictionary<int, FoundationMeshInfo>();

        m_filter = m_foundationObject.GetComponent<MeshFilter>();
        if (m_filter.sharedMesh == null)
        {
            m_filter.sharedMesh = new Mesh();
            m_mesh = m_filter.sharedMesh;
        }
    }
	
    void Update()
    {
        UpdateFoundation();
        HandleInput();
    }

    private void OnEnable()
    {
        MouseManager.Instance.SetActivePointer(true);
    }

    private void OnDisable()
    {
        MouseManager.Instance.SetActivePointer(false);
    }
    #endregion

    #region Input
    private void HandleInput()
    {
        Mode newMode = Input.GetKey(KeyCode.LeftControl) ? Mode.remove : Mode.add;
        if (newMode != m_currentMode)
        {
            //FlipWalls(newMode);
            m_currentMode = newMode;
        }
        if (!EventSystem.current.IsPointerOverGameObject())
            HandleMouseDown();
        HandleMouseUp();
    }

    private void HandleMouseDown()
    {
        if (Input.GetMouseButtonDown(0)) { CreateFoundation(); }
    }

    private void HandleMouseUp()
    {
        if (Input.GetMouseButtonUp(0)) { PlaceFoundation(); }
    }
    #endregion

    #region Foundation Creation
    public void CreateFoundation()
    {
        m_currentlyPlacing = true;
        m_startPosition = MouseManager.s_roundedPositionFloor;
        m_previousTiles = new int[0];
    }

    public void PlaceFoundation()
    {
        m_currentlyPlacing = false;
        /*
        if (m_previousTiles.Length > 0 && NetworkClient.active)
        {
            PlayerController.mine.CmdPlaceFoundation(Conversion.ToString(m_previousTiles), (int)m_currentMode);
        }
        bool newValue = m_currentMode == Mode.add;
        foreach (int index in m_previousTiles) { m_foundationTiles[index] = newValue; }
        */
        m_foundationTiles = TilesInRect(GetFoundationRect());

        RefreshAvailableTiles();
    }

    public void UpdateFoundation()
    {
        if (!m_currentlyPlacing)
            return;
        RebuildMesh(GetFoundationRect());
        
        // Remove previous ones first so user is only placing the current tiles
        // TODO: improve performance

        /*
        foreach (int index in m_previousTiles)
        {
            if (m_currentMode == Mode.add)
            { 
                if (!m_foundationTiles[index]) { RemoveFoundation(index); }
            }
            else
            {
                if (m_foundationTiles[index]) { AddFoundation(index); }
            }
        }
            
        foreach (int index in currentTiles)
        {
            if (m_currentMode == Mode.add) { AddFoundation(index); }
            else { RemoveFoundation(index); }
        }
        */
    }

    private void RebuildMesh(RectInt a_rect)
    {
        bool[] currentTiles = TilesInRect(a_rect);

        List<Vector3> vertices = new List<Vector3>(currentTiles.Length * 96);
        List<Vector2> uvs = new List<Vector2>(vertices.Count);
        List<int> triangles = new List<int>(currentTiles.Length * 144);

        int actualTile = 0;

        m_mesh.Clear();

        for (int i = 0; i < currentTiles.Length; i++)
        {
            if (!currentTiles[i])
                continue;

            bool hasUp = i / LotManager.length == LotManager.length - 1 || !currentTiles[i + LotManager.length];
            bool hasDown = i / LotManager.length == 0 || !currentTiles[i - LotManager.length];
            bool hasLeft = i % LotManager.width == 0 || !currentTiles[i - 1];
            bool hasRight = i % LotManager.width == LotManager.width - 1 || !currentTiles[i + 1];

            if (!hasUp && !hasDown && !hasLeft && !hasRight)
                continue;

            Vector3[] _vertices = FoundationMeshInfo.FoundationVertices();
            Vector2[] _uvs = FoundationMeshInfo.FoundationUvs();
            int[] _triangles = FoundationMeshInfo.FoundationTriangles();

            int xOffset = i % LotManager.width;
            int zOffset = i / LotManager.width;

            for (int j = 0; j < _vertices.Length; j++)
                _vertices[j] += new Vector3(xOffset, 0, zOffset);

            //int startIndex = actualTile * 16;
            for (int j = 0; j < _triangles.Length; j++)
               _triangles[j] += actualTile;

            vertices.AddRange(_vertices);
            uvs.AddRange(_uvs);

            if (hasDown)
                for (int g = 0; g < 18; g++)
                    triangles.Add(_triangles[g]);

            if (hasUp)
                for (int g = 18; g < 36; g++)
                    triangles.Add(_triangles[g]);

            if (hasLeft)
                for (int g = 36; g < 54; g++)
                    triangles.Add(_triangles[g]);

            if (hasRight)
                for (int g = 54; g < 72; g++)
                    triangles.Add(_triangles[g]);

            if (hasDown && hasLeft)
                for (int g = 72; g < 90; g++)
                    triangles.Add(_triangles[g]);

            if (hasDown && hasRight)
                for (int g = 90; g < 108; g++)
                    triangles.Add(_triangles[g]);

            if (hasUp && hasRight)
                for (int g = 108; g < 126; g++)
                    triangles.Add(_triangles[g]);

            if (hasUp && hasLeft)
                for (int g = 126; g < 144; g++)
                    triangles.Add(_triangles[g]);

            actualTile += 96;
        }

        m_mesh.SetVertices(vertices);
        m_mesh.SetUVs(0, uvs);
        m_mesh.SetTriangles(triangles, 0);
        m_mesh.RecalculateNormals();
    }

    public void AddFoundation(int index)
    {
        if (!m_foundationInfos.ContainsKey(index))
        {
            FoundationMeshInfo info = new FoundationMeshInfo(m_foundationObject.GetComponent<MeshFilter>(), index);
            m_foundationInfos.Add(index, info);
        }
        m_foundationInfos[index].AddFoundation();
        //FindObjectOfType<FloorToolManager>().SetTile(index, 2); // currently doesn't work because no floor index info passed
    }

    public void RemoveFoundation(int index)
    {
        if (m_foundationInfos.ContainsKey(index))
            m_foundationInfos[index].RemoveFoundation();
        // undo tile
    }

    private RectInt GetFoundationRect()
    {
        RectInt result = new RectInt();
        int minX, maxX, minY, maxY;
        if (MouseManager.s_roundedPositionFloor.x < m_startPosition.x)
        {
            minX = (int)MouseManager.s_roundedPositionFloor.x;
            maxX = (int)m_startPosition.x;
        }
        else
        {
            minX = (int)m_startPosition.x;
            maxX = (int)MouseManager.s_roundedPositionFloor.x;
        }

        if (MouseManager.s_roundedPositionFloor.z < m_startPosition.z)
        {
            minY = (int)MouseManager.s_roundedPositionFloor.z;
            maxY = (int)m_startPosition.z;
        }
        else
        {
            minY = (int)m_startPosition.z;
            maxY = (int)MouseManager.s_roundedPositionFloor.z;
        }

        result.min = new Vector2Int(minX, minY);
        result.max = new Vector2Int(maxX, maxY);

        return result;
    }

    // TODO: might need to be optimized
    private bool[] TilesInRect(RectInt a_rect)
    {
        /*
        OLD SYSTEM
        int[] result = new int[a_rect.width * a_rect.height];
        for (int i = 0; i < a_rect.height; i++)
            for (int j = 0; j < a_rect.width; j++)
                result[j + i * a_rect.width] = a_rect.xMin + j + (a_rect.yMin + i) * LotManager.width;
        return result;
        */

        bool[] result = new bool[m_foundationTiles.Length];
        for (int i = 0; i < m_foundationTiles.Length; i++)
            result[i] = m_foundationTiles[i];

        for (int i = 0; i < a_rect.height; i++)
        {
            int baseIndex = a_rect.xMin + (a_rect.yMin + i) * LotManager.width;
            for (int j = 0; j < a_rect.width; j++)
                result[baseIndex + j] = m_currentMode == Mode.add;
        }
        return result;
    }
    #endregion

    #region Networking
    // TODO: need an idea for this system
    public void ActionUpdateFoundation(string a_indices, int a_tool)
    {
        
    }

    public void ActionPlaceFoundation(string a_indices, int a_tool)
    {
        bool newValue = a_tool == (int)Mode.add;
        foreach (string index in a_indices.Split(','))
        {
            if (string.IsNullOrEmpty(index))
                continue;
            int i = int.Parse(index);
            if (newValue)
                AddFoundation(i);
            else
                RemoveFoundation(i);
            m_foundationTiles[i] = newValue;
        }
        RefreshAvailableTiles();
    }
    #endregion

    private void RefreshAvailableTiles()
    {
        LotManager.Instance.groundFloor.RefreshAvailableFoundationTiles(m_previousTiles, c_foundationHeight);
    }
    #endregion
}
