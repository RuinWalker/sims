﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: turn on/off sides based on location of foundation
public class FoundationMeshInfo
{
    #region Variables
    public int m_index;
    public MeshFilter m_filterComp;

    public int m_startVertexIndex
    {
        get { return m_indexHolder.m_startVertexIndex; }
        set { m_indexHolder.m_startVertexIndex = value; }
    }

    public int m_startTriangleIndex
    {
        get { return m_indexHolder.m_startTriangleIndex; }
        set { m_indexHolder.m_startTriangleIndex = value; }
    }

    public FoundationIndexHolder m_indexHolder;

    private int m_frontStartIndex;
    private int m_backStartIndex;
    private int m_leftStartIndex;
    private int m_rigthStartIndex;
    #endregion

    public FoundationMeshInfo(MeshFilter a_filterComp, int a_index)
    {
        m_indexHolder = new FoundationIndexHolder();
        m_filterComp = a_filterComp;
        m_index = a_index;
        m_startVertexIndex = -1;
    }

    public void AddFoundation()
    {
        if (m_startVertexIndex >= 0)
            return;

        m_startVertexIndex = m_filterComp.mesh.vertexCount;
        m_startTriangleIndex = m_filterComp.mesh.triangles.Length;

        m_filterComp.GetComponent<FoundationIndicesHolder>().AddIndexHolder(m_indexHolder, m_index);
        List<Vector3> vertices = new List<Vector3>(m_filterComp.mesh.vertices);
        List<Vector2> uvs = new List<Vector2>(m_filterComp.mesh.uv);
        List<int> triangles = new List<int>(m_filterComp.mesh.triangles);

        int startIndex = vertices.Count;
        int startIndexTris = triangles.Count;
        int xOffset = m_index % LotManager.width;
        int zOffset = m_index / LotManager.width;

        vertices.AddRange(FoundationVertices());
        uvs.AddRange(FoundationUvs());
        triangles.AddRange(FoundationTriangles());
        for (int i = startIndex; i < vertices.Count; i++) { vertices[i] += new Vector3(xOffset, 0, zOffset); }
        for (int i = startIndexTris; i < triangles.Count; i++) { triangles[i] += startIndex; }

        m_filterComp.mesh.vertices = vertices.ToArray();
        m_filterComp.mesh.uv = uvs.ToArray();
        m_filterComp.mesh.triangles = triangles.ToArray();
        m_filterComp.mesh.RecalculateNormals();
    }

    public void RemoveFoundation()
    {
        // remove vertices, uvs, triangles from mesh
        // update indices holders

        if (m_startVertexIndex < 0)
            return;

        List<Vector3> vertices = new List<Vector3>(m_filterComp.mesh.vertices);
        List<Vector2> uvs = new List<Vector2>(m_filterComp.mesh.uv);
        List<int> triangles = new List<int>(m_filterComp.mesh.triangles);

        vertices.RemoveRange(m_startVertexIndex, 16);
        uvs.RemoveRange(m_startVertexIndex, 16);
        triangles.RemoveRange(m_startTriangleIndex, 24);
        for (int i = m_startTriangleIndex; i < triangles.Count; i++)
        {
            triangles[i] -= 16;
        }
        m_filterComp.GetComponent<FoundationIndicesHolder>().RemoveIndexHolder(m_index);

        m_filterComp.mesh.triangles = triangles.ToArray(); // Needs to happen first. Otherwise, setting the vertices will return an error about vertices referenced in the triangle array missing from the vertices array.
        m_filterComp.mesh.vertices = vertices.ToArray();
        m_filterComp.mesh.uv = uvs.ToArray();

        m_filterComp.mesh.RecalculateNormals();

        m_startVertexIndex = -1;
    }

    #region FoundationBases
    public static Vector3[] FoundationVertices()
    {
        Vector3[] vertices = new Vector3[96];

        float thickness = 0.1f;
        float halfThickness = thickness / 2;
        // Front
        vertices[0] = new Vector3(0, 0, -halfThickness);
        vertices[1] = new Vector3(0, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[2] = new Vector3(1, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[3] = new Vector3(1, 0, -halfThickness);

        vertices[4] = new Vector3(1, 0, halfThickness);
        vertices[5] = new Vector3(1, FoundationTool.c_foundationHeight, halfThickness);
        vertices[6] = new Vector3(0, FoundationTool.c_foundationHeight, halfThickness);
        vertices[7] = new Vector3(0, 0, halfThickness);

        vertices[8] = new Vector3(0, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[9] = new Vector3(0, FoundationTool.c_foundationHeight, halfThickness);
        vertices[10] = new Vector3(1, FoundationTool.c_foundationHeight, halfThickness);
        vertices[11] = new Vector3(1, FoundationTool.c_foundationHeight, -halfThickness);

        // Back
        vertices[12] = vertices[4] + new Vector3(0, 0, 1);
        vertices[13] = vertices[5] + new Vector3(0, 0, 1);
        vertices[14] = vertices[6] + new Vector3(0, 0, 1);
        vertices[15] = vertices[7] + new Vector3(0, 0, 1);

        vertices[16] = vertices[0] + new Vector3(0, 0, 1);
        vertices[17] = vertices[1] + new Vector3(0, 0, 1);
        vertices[18] = vertices[2] + new Vector3(0, 0, 1);
        vertices[19] = vertices[3] + new Vector3(0, 0, 1);

        vertices[20] = vertices[8] + new Vector3(0, 0, 1);
        vertices[21] = vertices[9] + new Vector3(0, 0, 1);
        vertices[22] = vertices[10] + new Vector3(0, 0, 1);
        vertices[23] = vertices[11] + new Vector3(0, 0, 1);

        // Side 1 (left)
        vertices[24] = new Vector3(-halfThickness, 0, 1);
        vertices[25] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[26] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[27] = new Vector3(-halfThickness, 0, 0);

        vertices[28] = new Vector3(halfThickness, 0, 0);
        vertices[29] = new Vector3(halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[30] = new Vector3(halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[31] = new Vector3(halfThickness, 0, 1);

        vertices[32] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[33] = new Vector3(halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[34] = new Vector3(halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[35] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 0);

        // Side 2 (right)
        vertices[36] = vertices[24] + new Vector3(1, 0, 0);
        vertices[37] = vertices[25] + new Vector3(1, 0, 0);
        vertices[38] = vertices[26] + new Vector3(1, 0, 0);
        vertices[39] = vertices[27] + new Vector3(1, 0, 0);

        vertices[40] = vertices[28] + new Vector3(1, 0, 0);
        vertices[41] = vertices[29] + new Vector3(1, 0, 0);
        vertices[42] = vertices[30] + new Vector3(1, 0, 0);
        vertices[43] = vertices[31] + new Vector3(1, 0, 0);

        vertices[44] = vertices[32] + new Vector3(1, 0, 0);
        vertices[45] = vertices[33] + new Vector3(1, 0, 0);
        vertices[46] = vertices[34] + new Vector3(1, 0, 0);
        vertices[47] = vertices[35] + new Vector3(1, 0, 0);

        // Front Left Corner
        vertices[48] = new Vector3(-halfThickness, 0, -halfThickness);
        vertices[49] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[50] = new Vector3(0, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[51] = new Vector3(0, 0, -halfThickness);

        vertices[52] = new Vector3(-halfThickness, 0, 0);
        vertices[53] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[54] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[55] = new Vector3(-halfThickness, 0, -halfThickness);

        vertices[56] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[57] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[58] = new Vector3(0, FoundationTool.c_foundationHeight, 0);
        vertices[59] = new Vector3(0, FoundationTool.c_foundationHeight, -halfThickness);

        // Front Right Corner
        vertices[60] = new Vector3(1, 0, -halfThickness);
        vertices[61] = new Vector3(1, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[62] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[63] = new Vector3(1 + halfThickness, 0, -halfThickness);

        vertices[64] = new Vector3(1 + halfThickness, 0, -halfThickness);
        vertices[65] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[66] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[67] = new Vector3(1 + halfThickness, 0, 0);

        vertices[68] = new Vector3(1, FoundationTool.c_foundationHeight, -halfThickness);
        vertices[69] = new Vector3(1, FoundationTool.c_foundationHeight, 0);
        vertices[70] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[71] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, -halfThickness);

        // Back Right Corner
        vertices[72] = new Vector3(1 + halfThickness, 0, 1);
        vertices[73] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[74] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[75] = new Vector3(1 + halfThickness, 0, 1 + halfThickness);

        vertices[76] = new Vector3(1 + halfThickness, 0, 1 + halfThickness);
        vertices[77] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[78] = new Vector3(1, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[79] = new Vector3(1, 0, 1 + halfThickness);

        vertices[80] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[81] = new Vector3(1 + halfThickness, FoundationTool.c_foundationHeight, 0);
        vertices[82] = new Vector3(1, FoundationTool.c_foundationHeight, 0);
        vertices[83] = new Vector3(1, FoundationTool.c_foundationHeight, 1 + halfThickness);

        // Front Right Corner
        vertices[84] = new Vector3(0, 0, 1 + halfThickness);
        vertices[85] = new Vector3(0, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[86] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[87] = new Vector3(-halfThickness, 0, 1 + halfThickness);

        vertices[88] = new Vector3(-halfThickness, 0, 1 + halfThickness);
        vertices[89] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[90] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[91] = new Vector3(-halfThickness, 0, 1);

        vertices[92] = new Vector3(0, FoundationTool.c_foundationHeight, 1 + halfThickness);
        vertices[93] = new Vector3(0, FoundationTool.c_foundationHeight, 1);
        vertices[94] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1);
        vertices[95] = new Vector3(-halfThickness, FoundationTool.c_foundationHeight, 1 + halfThickness);

        return vertices;
    }

    public static Vector2[] FoundationUvs()
    {
        // TODO: FIX corner UVS
        Vector2[] uvs = new Vector2[96];

        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(0, 1);
        uvs[2] = new Vector2(1, 1);
        uvs[3] = new Vector2(1, 0);

        for (int i = 4; i < 96; i += 4)
        {
            uvs[i] = uvs[0];
            uvs[i + 1] = uvs[1];
            uvs[i + 2] = uvs[2];
            uvs[i + 3] = uvs[3];
        }

        return uvs;
    }

    public static int[] FoundationTriangles()
    {
        int[] triangles = new int[8 * 3 * 6];

        for (int i = 0; i < 8 * 3; i++)
        {
            int triangleIndex = i * 6;
            int vertexIndex = i * 4;
            triangles[triangleIndex] = vertexIndex;
            triangles[triangleIndex + 1] = vertexIndex + 1;
            triangles[triangleIndex + 2] = vertexIndex + 2;
            triangles[triangleIndex + 3] = vertexIndex + 2;
            triangles[triangleIndex + 4] = vertexIndex + 3;
            triangles[triangleIndex + 5] = vertexIndex;
        }

        return triangles;
    }
    #endregion
}
