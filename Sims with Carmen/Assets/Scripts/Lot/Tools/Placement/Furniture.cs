using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class Furniture : MonoBehaviour
{
    #region Variables
    #region Public
    /// <summary>
    /// Whether this MCO is unique (this specific instance has already been placed in the stage) or not.
    /// </summary>
    public bool Unique { get; set; }
    /// <summary>
    /// Id of this furniture instance
    /// </summary>
    public int InstanceId { get; set; }
    /// <summary>
    /// Floor that this furniture is currently on
    /// </summary>
    public Floor Floor { get; set; }

    /// <summary>
    /// Whether this MCO snaps to a ceiling
    /// </summary>
    public bool SnapToCeiling { get { return snapToCeiling; } }

    public FurnitureData data { get { return m_data; } }
    #endregion

    #region Editor
    //TODO: description
    [SerializeField]
    private FurnitureData m_data;

    /// <summary>
    /// Should this object be positioned to align to the ceiling.
    /// </summary>
    [Tooltip("Should this object be positioned to align to the ceiling.")]
    [SerializeField]
    [FormerlySerializedAs("m_snapToCeiling")]
    private bool snapToCeiling;
    /// <summary>
    /// The target size of the scale animation upon placement.
    /// </summary>
    [Tooltip("The target size of the scale animation upon placement.")]
    [SerializeField]
    [FormerlySerializedAs("m_onPlaceScale")]
    private float scaleOnPlacement = 1.1f;
    /// <summary>
    /// The duration of the scale animation upon placement.
    /// </summary>
    [Tooltip("The duration of the scale animation upon placement.")]
    [SerializeField]
    [FormerlySerializedAs("m_onPlaceScaleTime")]
    private float scaleAnimationDuration = 0.2f;
    #endregion

    #endregion

    #region Methods
    #region Public
    /// <summary>
    /// Called upon placement. Runs Animation
    /// </summary>
    public void OnPlace()
    {
        Unique = true;
        StartCoroutine(ScaleEffectRoutine());
    }
    #endregion

    #region Unity
    /// <summary>
    /// Called when this Script is Enabled
    /// Turns on any lights on this object
    /// </summary>
    private void OnEnable()
    {
        foreach (Light light in GetComponentsInChildren<Light>())
            light.enabled = true;
    }
    #endregion

    #region Private
    /// <summary>
    /// Scale up and down animation ran when placing objects.
    /// </summary>
    private IEnumerator ScaleEffectRoutine()
    {
        Vector3 startScale = transform.localScale;
        Vector3 targetScale = startScale * scaleOnPlacement;
        float scaleSpeed = (targetScale - startScale).magnitude / (scaleAnimationDuration / 2f);
        // Scale up
        while (transform.localScale != targetScale)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, scaleSpeed * Time.deltaTime);
            yield return null;
        }
        // Scale down
        while (transform.localScale != startScale)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, startScale, scaleSpeed * Time.deltaTime);
            yield return null;
        }
    }
    #endregion
    #endregion
}
