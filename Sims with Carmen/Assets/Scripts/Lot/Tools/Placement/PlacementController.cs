using nl.DTT.Randstad.Placement.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

// TODO: smooth movement

// NOTE: for faster development, the script is sorted in regions of functionality instead of stuff like Public and Private.

/*
OVERVIEW
Click on an existing object with the MCO script attached to select the object. TODO: Selected objects currently miss a highlight, instead you will see a debug message.
Double click on an existing object (even when it is already selected) to grab it. It will now follow the mouse.
Click while holding an object to place it.
Press escape while an object is selected to deselect it.
Press delete while an object is selected to delete it.
When the held prefab is not unique (see MCO -> IsUnique), placing an object places an copy instead.
*/
public class PlacementController : CachedBehaviour<PlacementController>
{

    #region MOUSE OBJECT
    /// <summary>
    /// The id of the Default layer.
    /// </summary>
    private const int c_defaultLayer = 14;
    /// <summary>
    /// The id of the Ignore Raycasts layer.
    /// </summary>
    private const int c_ignoreRaycastLayer = 2;

    /// <summary>
    /// The object the mouse is currently hovering over.
    /// </summary>
    private GameObject m_mouseHoverObject;
    /// <summary>
    /// The gameobject that is currently being held by the user and can be placed.
    /// </summary>
    public GameObject mouseObject { get; private set; }

    public bool hasMouseObject { get { return mouseObject != null; } }
    public bool hasMouseHoverObject { get { return m_mouseHoverObject != null; } }
    /// <summary>
    /// Returns whether the held object is unique (this specific instance already placed in the Stage before)
    /// </summary>
    public bool mouseObjectIsUnique
    {
        get
        {
            return hasMouseObject &&
                (mouseObject.GetComponent<Furniture>() == null || mouseObject.GetComponent<Furniture>().Unique) &&
                (mouseObject.GetComponent<MouseGroup>() == null || mouseObject.GetComponent<MouseGroup>().IsUnique);
        }
    }
    /// <summary>
    /// The position of the currently grabbed object at the time of grabbing it.
    /// </summary>
    private Vector3 m_grabbedObjectStartPos;

    /// <summary>
    /// Whether the mouse object has been set this frame.
    /// </summary>
    private bool m_mouseObjectSetThisFrame;

    /// <summary>
    /// Whether the movement of the mouse object is currently limited to a single axis (depending on an additional button held at the time).
    /// </summary>
    private bool mouseObjectLockedAxis;

    /// <summary>
    /// The position of the mouse object at the start of being locked to the Y axis.
    /// </summary>
    private Vector3 m_mouseObjectCachedPos;

    /// <summary>
    /// The parent object of placed objects.
    /// </summary>
    private Transform m_placedObjectsRoot; // TODO: should be whichever floor currently hitting

    /// <summary>
    /// Sets a copy of the passed object as the current mouse object. Object is not unique.
    /// </summary>
    /// <param name="a_objectToCopy"></param>
    public void SetMouseObjectCopy(GameObject a_objectToCopy)
    {
        SetMouseObject(a_objectToCopy, false);
    }

    /// <summary>
    /// Sets the passed object as the current mouse object. Object is unique.
    /// </summary>
    /// <param name="a_objectToGrab"></param>
    public void SetMouseObjectGrab(GameObject a_objectToGrab)
    {
        if (a_objectToGrab != null)
        {
            Furniture furnitureComp = a_objectToGrab.GetComponent<Furniture>();
            if (NetworkClient.active && furnitureComp != null)
                PlayerController.mine.CmdReplaceObject(PlayerController.mine.netId.Value, furnitureComp.Floor.index, furnitureComp.InstanceId);
        }
        SetMouseObject(a_objectToGrab, true);
    }

    /// <summary>
    /// Sets the passed object as the current mouse object. If a_isUnique is false, a copy of the object is used.
    /// </summary>
    /// <param name="a_object"></param>
    /// <param name="a_isUnique"></param>
    private void SetMouseObject(GameObject a_object, bool a_isUnique)
    {
        if (a_object == null || a_object.GetComponentInChildren<Furniture>() == null) return;
        ActionSelection();
        if (a_isUnique)
        {
            //DeleteObject(a_object); TODO: should only happen for all non client things
            mouseObject = a_object;
            m_grabbedObjectStartPos = a_object.transform.position;
        }
        else
        {
            mouseObject = CopyFurnitureGroup(a_object, false);
            if (mouseObject.GetComponent<Furniture>() == null && mouseObject.GetComponent<MouseGroup>() == null) { mouseObject.AddComponent<MouseGroup>(); }
        }

        // TODO: Make sure mouseObject's highlight is off.

        foreach (Transform trans in mouseObject.GetComponentsInChildren<Transform>()) { trans.gameObject.layer = c_ignoreRaycastLayer; }
        MouseGroup groupComp = mouseObject.GetComponent<MouseGroup>();
        if (mouseObject.GetComponent<MouseGroup>() != null) { groupComp.IsUnique = a_isUnique; }
        m_mouseObjectSetThisFrame = true;
    }

    /// <summary>
    /// Copies the object under the mouse in a fashion like the eyedrop tool.
    /// </summary>
    private void CopyObjectUnderMouse()
    {
        ClearMouseObject();
        if (MouseManager.s_hitObject == null)
            return;
        SetMouseObjectCopy(MouseManager.s_hitObject);
        m_eyedropToolActive = false;
    }

    /// <summary>
    /// Releases the currently held mouse object.
    /// </summary>
    public void ClearMouseObject()
    {
        if (mouseObjectIsUnique) { ClearMouseObjectUnique(); }
        else { ClearMouseObjectCopy(); }
    }

    /// <summary>
    /// Releases the currently held mouse object as a unique object at the last grab position.
    /// </summary>
    private void ClearMouseObjectUnique()
    {
        mouseObject.transform.position = m_grabbedObjectStartPos;
        PlaceMouseObject();
    }

    /// <summary>
    /// Releases the currently held mouse object as a copy, deleting it.
    /// </summary>
    private void ClearMouseObjectCopy()
    {
        Destroy(mouseObject);
        mouseObject = null;
    }

    public static int GenerateUniqueId(Floor a_floor)
    {
        int uniqueId = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
        while (a_floor.m_furniture.ContainsKey(uniqueId))
        {
            uniqueId = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
        }
        return uniqueId;
    }

    public void PlaceObject(GameObject a_placedObject, Floor a_floor)
    {
        PlaceObject(a_placedObject, a_floor, GenerateUniqueId(a_floor));
    }

    public void PlaceObject(GameObject a_placedObject, Floor a_floor, int a_uniqueId)
    {
        PlaceObject(a_placedObject, a_floor, a_placedObject.transform.position, a_placedObject.transform.rotation, a_uniqueId);
    }

    public void PlaceObject(int a_objectId, Floor a_floor, Vector3 a_location, Quaternion a_rotation, int a_uniqueId)
    {
        GameObject placedObject = Instantiate(FurnitureDataHolder.data[a_objectId].prefab, a_location, a_rotation);
        PlaceObject(placedObject, a_floor, a_uniqueId);
    }

    // TODO: Handle grouped objects correctly with instance id and stuff
    public void PlaceObject(GameObject a_placedObject, Floor a_floor, Vector3 a_position, Quaternion a_rotation, int a_uniqueId)
    {
        a_placedObject.transform.position = a_position;
        a_placedObject.transform.rotation = a_rotation;

        ParentObjectToRoot(a_placedObject);

        foreach (Transform trans in a_placedObject.GetComponentsInChildren<Transform>())
            trans.gameObject.layer = c_defaultLayer;

        Furniture[] furnitureComps = a_placedObject.GetComponentsInChildren<Furniture>();
        foreach (Furniture furniture in furnitureComps)
        {
            furniture.Unique = true;
            furniture.OnPlace();
        }

        if (a_floor != null)
        {
            a_floor.m_furniture.Add(a_uniqueId, a_placedObject);
            foreach (Furniture furniture in furnitureComps)
            {
                furniture.Floor = a_floor;
                furniture.InstanceId = a_uniqueId;
            }
        }
    }

    /// <summary>
    /// Places the currently held mouse object at the current mouse position.
    /// </summary>
    private void PlaceMouseObject()
    {
        if (!hasMouseObject)
            return;
        Furniture furnitureComp = mouseObject.GetComponent<Furniture>();
        if (furnitureComp == null)
            return;

        if (NetworkClient.active)
        {
            PlayerController.mine.CmdPlaceObject(furnitureComp.data.id, MouseManager.s_floor.index, mouseObject.transform.position, mouseObject.transform.rotation);
            if (mouseObjectIsUnique)
            {
                Destroy(mouseObject);
                mouseObject = null;
            }
        }
        else
        {
            GameObject placedObject;
            if (mouseObjectIsUnique)
            {
                placedObject = mouseObject;
                mouseObject = null;
            }
            else
                placedObject = CopyFurnitureGroup(mouseObject, true);
            PlaceObject(placedObject, MouseManager.s_floor);
        }  

        //if (HasObjectSelected(placedObject) || !placedObject.GetComponent<MouseGroup>())
        //{
        //    return;
        //}

        //// If the object is the currently selected object, ungroup all children and delete the selection object. Else, they will stay grouped.
        //Transform[] children = new Transform[placedObject.transform.childCount];
        //for (int i = 0; i < children.Length; i++)
        //    children[i] = placedObject.transform.GetChild(i);
        //foreach (Transform trans in children)
        //    if (trans != placedObject.transform)
        //        ParentObjectToRoot(trans.gameObject);
        //Destroy(placedObject);
    }

    /// <summary>
    /// Rotates the mouse object by the specified angle.
    /// </summary>
    /// <param name="a_angle"></param>
    private void RotateMouseObject(float a_angle)
    {
        if (!hasMouseObject)
            return;
        mouseObject.transform.Rotate(new Vector3(0, a_angle, 0));
    }

    /// <summary>
    /// Instaniates a copy of the suplied GameObject and initializes the MCO components, if any, using the data from the corresponding component in the source object.
    /// </summary>
    /// <param name="a_objectToCopy"></param>
    /// <param name="a_isUnique"></param>
    /// <returns></returns>
    private static GameObject CopyFurnitureGroup(GameObject a_objectToCopy, bool a_isUnique)
    {
        GameObject result = Instantiate(a_objectToCopy);
        /*
        Furniture[] sourceFurnitures = a_objectToCopy.GetComponentsInChildren<Furniture>();
        Furniture[] resultFurnitures = result.GetComponentsInChildren<Furniture>();
        for (int i = 0; i < sourceFurnitures.Length; ++i)
            resultFurnitures[i].Initialize(sourceFurnitures[i].ReferenceData, a_isUnique);
            */
        return result;
    }

    /// <summary>
    /// Parents the supplied object to the root object for all placed modular content.
    /// </summary>
    /// <param name="a_object"></param>
    private void ParentObjectToRoot(GameObject a_object)
    {
        a_object.transform.SetParent(m_placedObjectsRoot);
    }

    /// <summary>
    /// Deletes the object from the stage.
    /// </summary>
    /// <param name="a_object"></param>
    public void DeleteObject(GameObject a_object)
    {
        if (a_object == null) { return; }
        Furniture furnitureComp = a_object.GetComponent<Furniture>();
        if (furnitureComp == null)
            return;
        if (furnitureComp.Floor == null)
            DestroyImmediate(a_object);
        else
        {
            if (NetworkClient.active)
                FindObjectOfType<PlayerController>().CmdDeleteObject(furnitureComp.Floor.index, furnitureComp.InstanceId);
            else
                DeleteObject(furnitureComp.Floor, furnitureComp.InstanceId);
        }
    }

    public void DeleteObject(Floor a_floor, int a_uniqueId)
    {
        if (!a_floor.m_furniture.ContainsKey(a_uniqueId))
            return;
        GameObject obj = a_floor.m_furniture[a_uniqueId];
        a_floor.m_furniture.Remove(a_uniqueId);
        DestroyImmediate(obj);
    }

    /// <summary>
    /// Show the mouse hover effect
    /// </summary>
    /// <param name="a_object"></param>
    private void ShowHoverOutline(GameObject a_object)
    {
        m_mouseHoverObject = a_object;
        // TODO: this
    }

    /// <summary>
    /// Hide the mouse hover effect
    /// </summary>
    private void HideHoverOutline()
    {
        if (!hasMouseHoverObject) { return; }
        m_mouseHoverObject = null;
    }

    /// <summary>
    /// handle the mouse hover effect
    /// </summary>
    private void HandleMouseHover()
    {
        if (MouseManager.s_hitObject == null)
        {
            HideHoverOutline();
            return;
        }
        if (hasMouseHoverObject)
        {
            if (MouseManager.s_hitObject == m_mouseHoverObject)
                return;
            HideHoverOutline();
        }
        if (hasMouseObject || EventSystem.current.IsPointerOverGameObject() || Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2)) return;
        ShowHoverOutline(MouseManager.s_hitObject);
    }

    #endregion

    #region UPDATE
    private void Update()
    {
        HandleInput();
        UpdateMouseObject();
        HandleMouseHover();
        m_mouseObjectSetThisFrame = false;
    }

    // TODO: update this
    /// <summary>
    /// Updates the location of the currently held mouse object.
    /// </summary>
    private void UpdateMouseObject()
    {
        if (!hasMouseObject || Input.GetMouseButton(0))
            return;
        // Mark current hovered tiles
        // Using a boolean because otherwise m_mouseObjectCachedPos would be set wrong. Additionally, allows to expand in a toggle based tool.

        mouseObject.transform.position = MouseManager.s_tilePositionFloor + new Vector3(0.5f, 0, 0.5f);
    }

    /// <summary>
    /// Updates the location of the currently held mouse object of the currently locked axes.
    /// </summary>
    private void UpdateMouseObjectLockedAxis()
    {
        Vector3 targetPosition = m_mouseObjectCachedPos;
        bool inputPressedFlag = false;
        //Vector3 worldPos = MouseWorldPos;

        if (Input.GetKey(KeyCode.X))
        {
            inputPressedFlag = true;
            //targetPosition.x = worldPos.x;
        }
        if (Input.GetKey(KeyCode.Z))
        {
            inputPressedFlag = true;
            //targetPosition.z = worldPos.z;
        }
        if (Input.GetKey(KeyCode.Y) || !inputPressedFlag)
        {
            float rayDistance;
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            new Plane(new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z), mouseObject.transform.position).Raycast(mouseRay, out rayDistance);
            targetPosition.y = mouseRay.GetPoint(rayDistance).y + m_mouseObjectCachedPos.y;
            //if (Input.GetKey(m_snappingKey)) targetPosition.y = DTTMath.RoundToMultiple(targetPosition.y, Foundation.Instance.GridGranularity);
        }
        mouseObject.transform.position = targetPosition;
    }

    /// <summary>
    /// Turns the highlight on the object off.
    /// </summary>
    /// <param name="a_object"></param>
    private static void SetObjectHighlight(GameObject a_object)
    {
    }
    #endregion

    #region INPUT 
    /// <summary>
    /// Whether the deletion tool is currently active.
    /// </summary>
    private bool m_deletionToolActive;
    /// <summary>
    /// Whether the eyedrop tool is currently active.
    /// </summary>
    private bool m_eyedropToolActive;
    /// <summary>
    /// The time at which the user started the click.
    /// </summary>
    private float m_leftClickFlag;
    /// <summary>
    /// The time for a click to be counted as a hold.
    /// </summary>
    private float m_holdTime = 0.5f;

    /// <summary>
    /// Calls all input handler functions.
    /// </summary>
    private void HandleInput()
    {
        HandleMouse();
        HandleShortcuts();
    }

    /// <summary>
    /// Calls handler functions for each pressed shortcut.
    /// </summary>
    private void HandleShortcuts()
    {
        if (Input.GetKeyDown(KeyCode.Comma)) ActionRotateClockwise();
        if (Input.GetKeyDown(KeyCode.Period)) ActionRotateCounterClockwise();
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.H)) { ActionSelection(); }
        if (Input.GetKeyDown(KeyCode.Delete)) ActionDelete();
        if (Input.GetKeyDown(KeyCode.K)) ActionDeleteTool();
        if (Input.GetKeyDown(KeyCode.I)) ActionEyedropTool();
    }

    /// <summary>
    /// Calls all mouse input handler functions.
    /// </summary>
    private void HandleMouse()
    {
        HandleLeftMouse();
        HandleRightMouse();
    }

    // In the next two functions, I am trying out falling back on HandeLeftMouseClick if HandleLeftMouseDoubleClick is unsuccessful.
    // The commented code is the old flag setting for double clicking.
    /// <summary>
    /// Handles clicks input from the left mouse button.
    /// </summary>
    private void HandleLeftMouse()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.GetMouseButtonDown(0))
        {
            HandleLeftMouseDown();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            HandleLeftMouseClick();
        }
        else if (Input.GetMouseButton(0))
            if (Time.time - m_leftClickFlag > m_holdTime)
            {
                HandleLeftMouseHold();
            }
    }

    /// <summary>
    /// Handles input from the right mouse button.
    /// </summary>
    private void HandleRightMouse()
    {
        if (Input.GetMouseButtonDown(1))
        {
            HandleLeftMouseDown();
        }
        if (Input.GetMouseButtonDown(1))
        {
            HandleRightMouseClick();
        }
    }

    private void HandleLeftMouseDown()
    {
        m_leftClickFlag = Time.time;
    }

    /// <summary>
    /// Handles left mouse clicks.
    /// </summary>
    private void HandleLeftMouseClick()
    {
        //Foundation.Instance.SelectedTile.SetActive(false);
        if (m_deletionToolActive && HitFurniture != null) { DeleteObject(MouseManager.Instance.hitObject); }
        else if (m_eyedropToolActive) { CopyObjectUnderMouse(); }
        else if (hasMouseObject)
        {
            // Separate if statement because we don't want the mouse object changed.
            if (!m_mouseObjectSetThisFrame)
                PlaceMouseObject();
        }
        else
        {
            SetMouseObjectGrab(MouseManager.Instance.hitObject);
        }
    }

    /// <summary>
    /// Handles hold inputs with the left mouse
    /// </summary>
    private void HandleLeftMouseHold()
    {
        if (!hasMouseObject)
            return;
        Vector3 diffVector = MouseManager.Instance.castInfoFloor.point - mouseObject.transform.position;
        float angle = Vector3.SignedAngle(Vector3.forward, diffVector, Vector3.up);
        angle = MathExtensions.RoundToMultiple(angle, 90);
        mouseObject.transform.eulerAngles = new Vector3(0, angle, 0);
    }

    /// <summary>
    /// Handles right mouse clicks.
    /// </summary>
    private void HandleRightMouseClick()
    {
        ActionSelection();
    }

    private void HandleShiftDown()
    {
        if (!hasMouseObject)
            return;
        m_mouseObjectCachedPos = mouseObject.transform.position;
        mouseObjectLockedAxis = true;
    }

    private void HandleShiftUp()
    {
        mouseObjectLockedAxis = false;
    }
    #endregion

    #region ACTIONS
    private void ClearToolFlags()
    {
        m_deletionToolActive = false;
        m_eyedropToolActive = false;
    }

    /// <summary>
    /// Deactivates all tools, mouse object or anything else that hinders object selection.
    /// </summary>
    public void ActionSelection()
    {
        ClearToolFlags();
        ClearMouseObject();
    }

    /// <summary>
    /// Rotates the object clockwise.
    /// </summary>
    public void ActionRotateClockwise()
    {
        RotateMouseObject(-90);
    }

    /// <summary>
    /// Rotates the object counter clockwise.
    /// </summary>
    public void ActionRotateCounterClockwise()
    {
        RotateMouseObject(90);
    }

    /// <summary>
    /// Attempts to delete the selected object. If not possible, select the delete tool.
    /// </summary>
    public void ActionDelete()
    {
        if (hasMouseObject)
            DeleteObject(mouseObject);
        else
            ActionDeleteTool();
    }

    /// <summary>
    /// Deletes the currently selected object. (TODO: If no object is currently selected, switches to Delete tool instead.)
    /// </summary>
    public void ActionDeleteTool()
    {
        ActionSelection();
        m_deletionToolActive = true;
    }

    /// <summary>
    /// Copies a existing object to the mouse object.
    /// </summary>
    public void ActionEyedropTool()
    {
        ActionSelection();
        m_eyedropToolActive = true;
    }
    #endregion

    #region RAYCASTING
    #region Variables
    #region Static
    /// <summary>
    /// The MCO underneath the mouse, if any. Returns null if none.
    /// </summary>
    public static Furniture HitFurniture { get { return Instance.HitFurnitureInst; } }
    #endregion

    #region Public
    /// <summary>
    /// The MCO underneath the mouse, if any. Returns null if none.
    /// </summary>
    public Furniture HitFurnitureInst { get { return MouseManager.s_hitObject != null ? MouseManager.s_hitObject.GetComponent<Furniture>() : null;} }
    #endregion
    #endregion

    #endregion
}