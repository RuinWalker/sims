﻿using UnityEngine;

namespace nl.DTT.Randstad.Placement.Objects
{
    public class MouseGroup : MonoBehaviour
    {
        #region Variables
        /// <summary>
        /// Whether the object is Unique
        /// </summary>
        public bool IsUnique;
        #endregion
    }
}
