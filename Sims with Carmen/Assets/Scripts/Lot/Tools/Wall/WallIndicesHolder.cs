﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallIndexHolder
{
    public int m_startVertexIndex;
    public int m_startTriangleIndex;
}

public class WallIndicesHolder : MonoBehaviour {

    Dictionary<int, WallIndexHolder> m_horzIndexHolders;
    Dictionary<int, WallIndexHolder> m_vertIndexHolders;

    private void Awake()
    {
        m_horzIndexHolders = new Dictionary<int, WallIndexHolder>();
        m_vertIndexHolders = new Dictionary<int, WallIndexHolder>();
    }

    public void RemoveIndexHolder(int a_index, WallTool.WallDirection a_direction)
    {
        int startIndexVert = 0;
        int startIndexTri = 0;
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                startIndexVert = m_horzIndexHolders[a_index].m_startVertexIndex;
                startIndexTri = m_horzIndexHolders[a_index].m_startTriangleIndex;
                m_horzIndexHolders.Remove(a_index);
                break;
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                startIndexVert = m_vertIndexHolders[a_index].m_startVertexIndex;
                startIndexTri = m_vertIndexHolders[a_index].m_startTriangleIndex;
                m_vertIndexHolders.Remove(a_index);
                break;
        }
        foreach (WallIndexHolder holder in m_horzIndexHolders.Values)
        {
            if (holder.m_startVertexIndex > startIndexVert)
            {
                holder.m_startVertexIndex -= 20;
            }
            if (holder.m_startTriangleIndex > startIndexTri)
            {
                holder.m_startTriangleIndex -= 30;
            }
        }

        foreach (WallIndexHolder holder in m_vertIndexHolders.Values)
        {
            if (holder.m_startVertexIndex > startIndexVert)
            {
                holder.m_startVertexIndex -= 20;
            }
            if (holder.m_startTriangleIndex > startIndexTri)
            {
                holder.m_startTriangleIndex -= 30;
            }
        }
    }

    public void AddIndexHolder(WallIndexHolder a_holder, int a_index, WallTool.WallDirection a_direction)
    {
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                m_horzIndexHolders.Add(a_index, a_holder);
                break;
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                m_vertIndexHolders.Add(a_index, a_holder);
                break;
        }
    }
}
