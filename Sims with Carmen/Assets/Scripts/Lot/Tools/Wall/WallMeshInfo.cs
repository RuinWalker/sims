﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMeshInfo
{

    public int m_index;
    public MeshFilter m_filterComp;

    public int m_startVertexIndex
    {
        get { return m_indexHolder.m_startVertexIndex; }
        set { m_indexHolder.m_startVertexIndex = value; }
    }

    public int m_startTriangleIndex
    {
        get { return m_indexHolder.m_startTriangleIndex; }
        set { m_indexHolder.m_startTriangleIndex = value; }
    }

    public WallIndexHolder m_indexHolder;

    public WallTool.WallDirection m_direction;

    public WallMeshInfo(MeshFilter a_filterComp, int a_index, WallTool.WallDirection a_direction)
    {
        m_indexHolder = new WallIndexHolder();
        m_filterComp = a_filterComp;
        m_index = a_index;
        m_direction = a_direction;
        m_startVertexIndex = -1;
    }

    const float wallWidth = 0.15f;
    const float uvWidth = 0.5f;

    public void AddWall()
    {
        if (m_startVertexIndex >= 0)
            return;

        m_startVertexIndex = m_filterComp.mesh.vertexCount;
        m_startTriangleIndex = m_filterComp.mesh.triangles.Length;

        m_filterComp.GetComponent<WallIndicesHolder>().AddIndexHolder(m_indexHolder, m_index, m_direction);
        List<Vector3> vertices = new List<Vector3>(m_filterComp.mesh.vertices);
        List<Vector2> uvs = new List<Vector2>(m_filterComp.mesh.uv);
        List<int> triangles = new List<int>(m_filterComp.mesh.triangles);

        int startIndex = vertices.Count;
        int startIndexTris = triangles.Count;
        int xOffset;
        int zOffset;
        if (m_direction == WallTool.WallDirection.left || m_direction == WallTool.WallDirection.right)
        {
            vertices.AddRange(HorizontalWallVertices());
            xOffset = m_index % LotManager.width;
            zOffset = m_index / LotManager.width;
        }
        else
        {
            vertices.AddRange(VerticalWallVertices());
            xOffset = m_index % (LotManager.width + 1);
            zOffset = m_index / (LotManager.width + 1);
            
        }
        uvs.AddRange(HorizontalWallUvs());
        triangles.AddRange(HorizontalWallTriangles());
        for (int i = startIndex; i < vertices.Count; i++) { vertices[i] += new Vector3(xOffset, 0, zOffset); }
        for (int i = startIndexTris; i < triangles.Count; i++) { triangles[i] += startIndex; }

        m_filterComp.mesh.vertices = vertices.ToArray();
        m_filterComp.mesh.uv = uvs.ToArray();
        m_filterComp.mesh.triangles = triangles.ToArray();
        m_filterComp.mesh.RecalculateNormals();
    }

    public void RemoveWall()
    {
        if (m_startVertexIndex < 0)
            return;

        List<Vector3> vertices = new List<Vector3>(m_filterComp.mesh.vertices);
        List<Vector2> uvs = new List<Vector2>(m_filterComp.mesh.uv);
        List<int> triangles = new List<int>(m_filterComp.mesh.triangles);

        vertices.RemoveRange(m_startVertexIndex, 20);
        uvs.RemoveRange(m_startVertexIndex, 20);
        triangles.RemoveRange(m_startTriangleIndex, 30);
        for (int i = m_startTriangleIndex; i < triangles.Count; i++)
        {
            triangles[i] -= 20;
        }
        m_filterComp.GetComponent<WallIndicesHolder>().RemoveIndexHolder(m_index, m_direction);

        m_filterComp.mesh.triangles = triangles.ToArray(); // Needs to happen first. Otherwise, setting the vertices will return an error about vertices referenced in the triangle array missing from the vertices array.
        m_filterComp.mesh.vertices = vertices.ToArray();
        m_filterComp.mesh.uv = uvs.ToArray();
        
        m_filterComp.mesh.RecalculateNormals();

        m_startVertexIndex = -1;
    }

    #region WallBases
    public Vector3[] HorizontalWallVertices()
    {
        Vector3[] vertices = new Vector3[20];
        /*
        Vector2[] uvs = new Vector2[vertices.Length];
        int[] triangles = new int[10 * 3];
        */


        // Front
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, WallTool.c_wallHeight, 0);
        vertices[2] = new Vector3(1, WallTool.c_wallHeight, 0);
        vertices[3] = new Vector3(1, 0, 0);

        /*
        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(0, 1);
        uvs[2] = new Vector2(uvWidth, 1);
        uvs[3] = new Vector2(uvWidth, 0);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;

        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;
        */

        // Back
        vertices[4] = new Vector3(1, 0, wallWidth);
        vertices[5] = new Vector3(1, WallTool.c_wallHeight, wallWidth);
        vertices[6] = new Vector3(0, WallTool.c_wallHeight, wallWidth);
        vertices[7] = new Vector3(0, 0, wallWidth);

        /*
        uvs[4] = new Vector2(0, 0);
        uvs[5] = new Vector2(0, 1);
        uvs[6] = new Vector2(uvWidth, 1);
        uvs[7] = new Vector2(uvWidth, 0);

        triangles[6] = 4;
        triangles[7] = 5;
        triangles[8] = 6;

        triangles[9] = 6;
        triangles[10] = 7;
        triangles[11] = 4;
        */

        ///*
        // Top
        vertices[8] = new Vector3(0, WallTool.c_wallHeight, 0);
        vertices[9] = new Vector3(0, WallTool.c_wallHeight, wallWidth);
        vertices[10] = new Vector3(1, WallTool.c_wallHeight, wallWidth);
        vertices[11] = new Vector3(1, WallTool.c_wallHeight, 0);

        /*
        uvs[8] = new Vector2(0, 0);
        uvs[9] = new Vector2(0, 0);
        uvs[10] = new Vector2(0, 0);
        uvs[11] = new Vector2(0, 0);

        triangles[12] = 8;
        triangles[13] = 9;
        triangles[14] = 10;

        triangles[15] = 10;
        triangles[16] = 11;
        triangles[17] = 8;
        */
        //*/

        // Side 1 (left)
        vertices[12] = new Vector3(0, 0, wallWidth);
        vertices[13] = new Vector3(0, WallTool.c_wallHeight, wallWidth);
        vertices[14] = new Vector3(0, WallTool.c_wallHeight, 0);
        vertices[15] = new Vector3(0, 0, 0);

        /*
        uvs[12] = new Vector2(0, 0);
        uvs[13] = new Vector2(0, 1);
        uvs[14] = new Vector2(0.15f * uvWidth, 1);
        uvs[15] = new Vector2(0.15f * uvWidth, 0);

        triangles[18] = 12;
        triangles[19] = 13;
        triangles[20] = 14;

        triangles[21] = 14;
        triangles[22] = 15;
        triangles[23] = 12;
        */

        // Side 2 (right)
        vertices[16] = new Vector3(1, 0, 0);
        vertices[17] = new Vector3(1, WallTool.c_wallHeight, 0);
        vertices[18] = new Vector3(1, WallTool.c_wallHeight, wallWidth);
        vertices[19] = new Vector3(1, 0, wallWidth);

        /*
        uvs[16] = new Vector2(0, 0);
        uvs[17] = new Vector2(0, 1);
        uvs[18] = new Vector2(0.15f * uvWidth, 1);
        uvs[19] = new Vector2(0.15f * uvWidth, 0);

        triangles[24] = 16;
        triangles[25] = 17;
        triangles[26] = 18;

        triangles[27] = 18;
        triangles[28] = 19;
        triangles[29] = 16;
        */

        return vertices;
    }

    public Vector2[] HorizontalWallUvs()
    {
        Vector2[] uvs = new Vector2[20];

        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(0, 1);
        uvs[2] = new Vector2(uvWidth, 1);
        uvs[3] = new Vector2(uvWidth, 0);

        uvs[4] = new Vector2(0, 0);
        uvs[5] = new Vector2(0, 1);
        uvs[6] = new Vector2(uvWidth, 1);
        uvs[7] = new Vector2(uvWidth, 0);

        uvs[8] = new Vector2(0, 0);
        uvs[9] = new Vector2(0, 0);
        uvs[10] = new Vector2(0, 0);
        uvs[11] = new Vector2(0, 0);

        uvs[12] = new Vector2(0, 0);
        uvs[13] = new Vector2(0, 1);
        uvs[14] = new Vector2(0.15f * uvWidth, 1);
        uvs[15] = new Vector2(0.15f * uvWidth, 0);

        uvs[16] = new Vector2(0, 0);
        uvs[17] = new Vector2(0, 1);
        uvs[18] = new Vector2(0.15f * uvWidth, 1);
        uvs[19] = new Vector2(0.15f * uvWidth, 0);

        return uvs;
    }

    public int[] HorizontalWallTriangles()
    {
        int[] triangles = new int[10 * 3];

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;

        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        triangles[6] = 4;
        triangles[7] = 5;
        triangles[8] = 6;

        triangles[9] = 6;
        triangles[10] = 7;
        triangles[11] = 4;

        triangles[12] = 8;
        triangles[13] = 9;
        triangles[14] = 10;

        triangles[15] = 10;
        triangles[16] = 11;
        triangles[17] = 8;

        triangles[18] = 12;
        triangles[19] = 13;
        triangles[20] = 14;

        triangles[21] = 14;
        triangles[22] = 15;
        triangles[23] = 12;

        triangles[24] = 16;
        triangles[25] = 17;
        triangles[26] = 18;

        triangles[27] = 18;
        triangles[28] = 19;
        triangles[29] = 16;

        return triangles;
    }

    public Vector3[] VerticalWallVertices()
    {
        Vector3[] vertices = new Vector3[20];

        // Front
        vertices[0] = new Vector3(0, 0, 1);
        vertices[1] = new Vector3(0, WallTool.c_wallHeight, 1);
        vertices[2] = new Vector3(0, WallTool.c_wallHeight, 0);
        vertices[3] = new Vector3(0, 0, 0);

        // Back
        vertices[4] = new Vector3(wallWidth, 0, 0);
        vertices[5] = new Vector3(wallWidth, WallTool.c_wallHeight, 0);
        vertices[6] = new Vector3(wallWidth, WallTool.c_wallHeight, 1);
        vertices[7] = new Vector3(wallWidth, 0, 1);

        // Top
        vertices[8] = new Vector3(0, WallTool.c_wallHeight, 1);
        vertices[9] = new Vector3(wallWidth, WallTool.c_wallHeight, 1);
        vertices[10] = new Vector3(wallWidth, WallTool.c_wallHeight, 0);
        vertices[11] = new Vector3(0, WallTool.c_wallHeight, 0);

        // Side 1 (left)
        vertices[12] = new Vector3(0, 0, 0);
        vertices[13] = new Vector3(0, WallTool.c_wallHeight, 0);
        vertices[14] = new Vector3(wallWidth, WallTool.c_wallHeight, 0);
        vertices[15] = new Vector3(wallWidth, 0, 0);

        // Side 2 (right)
        vertices[16] = new Vector3(wallWidth, 0, 1);
        vertices[17] = new Vector3(wallWidth, WallTool.c_wallHeight, 1);
        vertices[18] = new Vector3(0, WallTool.c_wallHeight, 1);
        vertices[19] = new Vector3(0, 0, 1);

        return vertices;
    }

    #endregion
}
