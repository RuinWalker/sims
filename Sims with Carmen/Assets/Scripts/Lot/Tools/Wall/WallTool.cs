﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/*
Rules:
Snap to nearest vertex
Display tooltip
After start input, wall snaps to vertex nearest cursor
When going diagional, only 45 degrees.
Picks the closest angle between 0 and 45 degrees
When can't move on for some reason, stop, display reason and cursor can move on
On release and can't move, wall disappears
Wall can always go through other walls, but not foundations and objects
Wall flattens any terrain it goes over unless something else is in the way.
*/

/*
Rules:
When can't move on for some reason, stop, display reason and cursor can move on
Wall can always go through other walls, but not foundations and objects
Wall flattens any terrain it goes over unless something else is in the way.

Can floor:
Diagonal wall - On top of wall and 1 tile extend all around.
Straight wall - Two tiles extend on both sides of wall.
Closed circuit of walls - Over the entirety of the room
*/

/*
TODO NOTES:


Modular models should disable/enable side tris based on surrounding tiles
Modular models positions are inccorect
Get floor index when placing walls

Save things on tile index? Walls would be saved as an int, with every bit meaning whether there is a wall in a certain direction or not
*/

public class WallTool : MonoBehaviour {

    //public enum WallDirection { none, horizontal, vertical, diagonal };
    public enum WallDirection { none, left, right, forward, back, diagonal };
    public enum Mode { add, remove };

    #region Variables
    public const float c_wallHeight = 3f;

    private Vector3 m_startPos;

    [SerializeField]
    private Material m_standardMat;
    [SerializeField]
    private Material m_okMat;
    [SerializeField]
    private Material m_errorMat;

    [SerializeField]
    private GameObject m_pointerPrefab;
    private GameObject m_activePointer;

    private bool m_validWall;

    private WallDirection m_currentDirection = WallDirection.none;
    private int m_currentEndIndex;
    private int m_startIndex;
    private Floor m_currentFloor;
    private int m_currentSubFloorIndex;

    private Vector3 m_roundedPositionStraightened;
    private Vector3 m_lastConstructionPos;
    private List<int> m_currentIndices;

    private Mode m_currentMode;

    private bool m_creatingWall;

    private Dictionary<int, WallMeshInfo> currentWallInfosHorz { get { return m_currentFloor.m_wallInfosHorz[m_currentSubFloorIndex]; } }
    private Dictionary<int, WallMeshInfo> currentWallInfosVert { get { return m_currentFloor.m_wallInfosVert[m_currentSubFloorIndex]; } }

    #endregion

    #region Unity
    void Awake () {
        m_currentIndices = new List<int>();
        m_currentMode = Mode.add;
    }

    void Update()
    {
        UpdatePointerPosition();
        UpdateWallPosition();
        HandleInput();
    }

    private void OnEnable()
    {
        ShowPointer();
    }

    private void OnDisable()
    {
        HidePointer();
    }
    #endregion

    #region Input
    private void HandleInput()
    {
        Mode newMode = Input.GetKey(KeyCode.LeftControl) ? Mode.remove : Mode.add;
        if (newMode != m_currentMode)
        {
            FlipWalls(newMode);
            m_currentMode = newMode;
        }
        HandleMouseDown();
        HandleMouseUp();
    }

    private void FlipWalls(Mode a_newMode)
    {
        if (a_newMode == Mode.add) { foreach (int index in m_currentIndices) { AddWall(index); } }
        else { foreach (int index in m_currentIndices) { RemoveWall(index); } }
    }

    private void HandleMouseDown()
    {
        if (Input.GetMouseButtonDown(0)) { CreateWall(); }
    }

    private void HandleMouseUp()
    {
        if (Input.GetMouseButtonUp(0)) { PlaceWall(); }
    }
    #endregion

    #region Pointer
    private void ShowPointer()
    {
        m_activePointer = Instantiate(m_pointerPrefab);
    }

    private void HidePointer()
    {
        Destroy(m_activePointer);
    }

    private void UpdatePointerPosition()
    {
        // TODO: Pointer should extend past grid
        if (MathExtensions.IsBetween(MouseManager.s_roundedPositionFloor.x, 0, LotManager.width) &&
            MathExtensions.IsBetween(MouseManager.s_roundedPositionFloor.z, 0, LotManager.length))
        {
            m_activePointer.transform.position = MouseManager.s_roundedPositionFloor;
        }
    }
    #endregion

    #region Wall Creation

    #region Updates
    private void CreateWall()
    {
        m_creatingWall = true;
        m_validWall = true;
        m_startPos = MouseManager.s_roundedPositionFloor;
        m_currentFloor = MouseManager.s_floor;
        m_currentSubFloorIndex = MouseManager.s_subFloorIndex;
        //m_activeWall = Instantiate(wallPrefab);
        SetActiveWallMaterial(m_okMat);
    }

    private void PlaceWall()
    {
        m_creatingWall = false;
        //SetWallMaterial(m_activeWall, m_standardMat);
        if (m_currentIndices.Count == 0 || !m_validWall)
        {
            m_currentIndices.Clear();
            return;
        }
        else
        {
            if (NetworkClient.active)
            {
                string indicesString = Conversion.ToString(m_currentIndices);
                FindObjectOfType<PlayerController>().CmdPlaceWall(indicesString, (int)m_currentDirection, m_currentFloor.index, m_currentSubFloorIndex);
                m_currentIndices.Clear();
                return;
            }
            SetTiles(m_currentIndices, m_currentMode == Mode.add, m_currentDirection, m_currentFloor, m_currentSubFloorIndex);
            // TODO: diagonal
            RefreshAvailableTiles();
        }
        m_currentIndices.Clear();
    }

    private void UpdateWallPosition()
    {
        if (!m_creatingWall) return;
        Vector3 differenceVector = MouseManager.s_roundedPositionFloor - m_startPos;

        if (differenceVector.magnitude == 0)
        {
            ResetActiveWall();
            m_currentDirection = WallDirection.none;
            return;
        }

        // Calculate the angle of the wall in degrees
        float wallAngle = (WallAngle(m_startPos, m_startPos + differenceVector));
        // Determine direction based on angle
        WallDirection newDirection = DirectionOfWall(wallAngle);

        if (m_currentDirection != newDirection)
        {
            // Going in other direction, reset before restoring direction.
            ResetActiveWall();
            m_currentDirection = newDirection;
        }

        // Correct the actual mouse position to a position that locks the wall in a multiple of 45 degrees. // Temporarily 90 degrees
        m_roundedPositionStraightened = StraightenEndPos(m_currentDirection);
        m_activePointer.transform.position = m_roundedPositionStraightened;

        // Get the start and end wall indices for the current wall.
        m_startIndex = GetIndexForPosition(m_startPos, m_currentDirection);
        m_currentEndIndex = GetIndexForPosition(m_roundedPositionStraightened, m_currentDirection);

        // Find all wall indices for the current wall.
        List<int> indices = AllIndicesBetween(m_startIndex, m_currentEndIndex, m_currentDirection);

        // Add missing indices to the array
        for (int i = m_currentIndices.Count; i < indices.Count; i++)
        {
            if (!ExtendActiveWall(m_currentDirection)) { break; }
            if (m_currentMode == Mode.add) { AddWall(indices[i]); }
            else { RemoveWall(indices[i]); }
            m_currentIndices.Add(indices[i]);
        }
        // Remove all indices not in the array
        for (int i = m_currentIndices.Count - 1; i >= indices.Count; i--)
        {
            bool previous = CurrentArrayHasWall(m_currentIndices[i]);
            if (m_currentMode == Mode.add) { if (!previous) { RemoveWall(m_currentIndices[i]); } }
            else { if (previous) { AddWall(m_currentIndices[i]); } }

            m_currentIndices.RemoveAt(i);
        }
    }

    private bool CurrentArrayHasWall(int a_index)
    {
        return CurrentlyHasWall(a_index, m_currentDirection, m_currentSubFloorIndex);
    }

    public bool CurrentlyHasWall(int a_index, WallDirection a_direction, int a_subFloorIndex)
    {
        switch (a_direction)
        {
            case WallDirection.right:
            case WallDirection.left:
                return m_currentFloor.m_horizontalWallTiles[a_subFloorIndex][a_index];
            case WallDirection.forward:
            case WallDirection.back:
                return m_currentFloor.m_verticalWallTiles[a_subFloorIndex][a_index];
        }
        return false;
    }
    #endregion

    #region Calculations
    // TODO: if not on first floor, check if tiles underneath
    private bool CanPlaceWall(Vector3 a_startPosition, Vector3 a_endPosition)
    {
        return true;
    }

    private bool IsValidIndex(int a_index, WallDirection a_direction)
    {
        switch (a_direction)
        {
            case WallDirection.left:
            case WallDirection.right:
                return MathExtensions.IsBetween(a_index, 0, m_currentFloor.m_horizontalWallTiles.Count);
            case WallDirection.forward:
            case WallDirection.back:
                return MathExtensions.IsBetween(a_index, 0, m_currentFloor.m_verticalWallTiles.Count);
                // TODO: Diagonal walls
        }
        return false;
    }

    private int GetIndexForPosition(Vector3 a_position, WallDirection a_direction)
    {
        switch (a_direction)
        {
            case WallDirection.left:
            case WallDirection.right:
                return (int)a_position.x + (int)a_position.z * LotManager.width;
            case WallDirection.forward:
            case WallDirection.back:
                return (int)a_position.x + (int)a_position.z * (LotManager.width + 1);
            // TODO: diagonal walls
            default:
                return -1;
        }
    }

    float WallAngle(Vector3 a_startPos, Vector3 a_endPos)
    {
        Vector3 differenceVector = a_endPos - a_startPos;
        float wallAngle = Vector3.SignedAngle(Vector3.right, differenceVector, Vector3.up);
        wallAngle = MathExtensions.RoundToMultiple(wallAngle, 90);// 45); Temporarliy 90, as diagonal walls are broken
        return wallAngle;
    }

    private WallDirection DirectionOfWall(float a_angle)
    {
        switch (((int)a_angle) % 360)
        {
            case 0:
                return WallDirection.right;
            case 180:
            case -180:
                return WallDirection.left;
            case -90:
                return WallDirection.forward;
            case 90:
                return WallDirection.back;
            case 45:
            case 135:
                return WallDirection.diagonal;
        }
        return WallDirection.none;
    }

    private Vector3 DirectionToVector(WallDirection a_direction)
    {
        switch (a_direction)
        {
            case WallDirection.right:
                return Vector3.right;
            case WallDirection.left:
                return Vector3.left;
            case WallDirection.forward:
                return Vector3.forward;
            case WallDirection.back:
                return Vector3.back;
            case WallDirection.diagonal:
                return new Vector3(1, 0, 1);
        }
        return Vector3.zero;
    }

    private Vector3 StraightenEndPos(WallDirection a_direction)
    {
        Vector3 differenceVector = MouseManager.s_roundedPositionFloor - m_startPos;
        differenceVector.y = 0; // Walls always use height from start position
        // Prevent from snapping to the wrong vertex when the angle is 45 or 135 (or the negative variants)
        if (a_direction.Equals(WallDirection.diagonal))
        {
            if (differenceVector.x != 0 && differenceVector.z != 0)
            {
                if (Mathf.Abs(differenceVector.x) > Mathf.Abs(differenceVector.z))
                {
                    float sign = (differenceVector.z < 0 ? -1 : 1);
                    differenceVector.z = Mathf.Abs(differenceVector.x) * sign;
                }
                else
                {
                    float sign = (differenceVector.x < 0 ? -1 : 1);
                    differenceVector.x = Mathf.Abs(differenceVector.z) * sign;
                }
            }
        }
        else
        {
            if (a_direction == WallDirection.left || a_direction == WallDirection.right)
                differenceVector.z = 0;
            else
                differenceVector.x = 0;
        }
        return differenceVector + m_startPos;
    }

    private List<int> AllIndicesBetween(int a_start, int a_end, WallDirection a_direction)
    {
        if (a_end < a_start)
        {
            int temp = a_start;
            a_start = a_end;
            a_end = temp;
        }
        List<int> indices = new List<int>();
        switch (a_direction)
        {
            case WallDirection.left:
                for (int i = a_start; i < a_end; i++) { indices.Add(i); }
                indices.Reverse();
                break;
            case WallDirection.right:
                for (int i = a_start; i < a_end; i++) { indices.Add(i); }
                break;
            case WallDirection.forward:
                for (int i = a_start; i < a_end; i += LotManager.width + 1) { indices.Add(i); }
                break;
            case WallDirection.back:
                for (int i = a_start; i < a_end; i += LotManager.width + 1) { indices.Add(i); }
                indices.Reverse();
                break;
        }
        return indices;
    }

    private bool ExtendActiveWall(WallDirection a_wallDirection)
    {
        if (!m_creatingWall) return false;
        //if (m_activeWall == null) { return false; }
        if (a_wallDirection == WallDirection.none) { return false; }

        Vector3 directionVector = DirectionToVector(a_wallDirection);

        if (!CanPlaceWall(m_lastConstructionPos, m_lastConstructionPos + directionVector))
        {
            m_validWall = false;
            SetActiveWallMaterial(m_errorMat);
            return false;
        }
        //SetWallMaterial(m_activeWall, m_okMat);
        // check if has to flatten
        //      if yes, do and save
        m_lastConstructionPos += directionVector;

        // add to array
        // extend wall model

        return true;
    }
    #endregion

    #region Visuals
    #endregion

    #region Networking
    // TODO: Pass whether Add or Remove
    public void ActionPlaceWall(string a_indices, int a_direction, int a_floorIndex, int a_subFloorIndex)
    {
        // TODO: if floor index/subfloor index is one that does not yet exist, create floor
        List<int> indices = new List<int>();
        WallDirection direction = (WallDirection)a_direction;
        foreach (string index in a_indices.Split(','))
        {
            if (string.IsNullOrEmpty(index))
                continue;
            indices.Add(int.Parse(index));
            AddWall(indices[indices.Count - 1], direction, a_floorIndex, m_currentSubFloorIndex);
        }

        SetTiles(indices, true, direction, a_floorIndex, a_subFloorIndex);
        RefreshAvailableTiles(a_floorIndex, a_subFloorIndex);
    }
    #endregion

    void AddWall(int a_index)
    {
        AddWall(a_index, m_currentDirection, m_currentFloor, m_currentSubFloorIndex);
    }

    // Need to pass subfloor index and floor index
    /*
    void AddWall(int a_index, WallDirection a_direction)
    {
        MeshFilter meshFilter = m_currentFloor.m_wallObjects[m_currentSubFloorIndex].transform.GetChild(0).GetComponent<MeshFilter>();
        WallMeshInfo result = new WallMeshInfo(meshFilter, a_index, a_direction);
        switch (a_direction)
        {
            case WallDirection.left:
            case WallDirection.right:
                if (currentWallInfosHorz.ContainsKey(a_index)) { currentWallInfosHorz[a_index].AddWall(); }
                else
                {
                    result.AddWall();
                    currentWallInfosHorz.Add(result.m_index, result);
                }
                break;
            case WallDirection.forward:
            case WallDirection.back:
                if (currentWallInfosVert.ContainsKey(a_index)) { currentWallInfosVert[a_index].AddWall(); }
                else
                {
                    result.AddWall();
                    currentWallInfosVert.Add(result.m_index, result);
                }
                break;
        }
    }
    */
    public static void AddWall(int a_index, WallDirection a_direction, int a_floorIndex, int a_subfloorIndex)
    {
        AddWall(a_index, a_direction, LotManager.Instance.GetFloor(a_floorIndex), a_subfloorIndex);
    }

    public static void AddWall(int a_index, WallDirection a_direction, Floor a_floor, int a_subfloorIndex)
    {
        MeshFilter meshFilter = a_floor.m_wallObjects[a_subfloorIndex].transform.GetChild(0).GetComponent<MeshFilter>();
        WallMeshInfo result = new WallMeshInfo(meshFilter, a_index, a_direction);
        Dictionary<int, WallMeshInfo> currentWallInfos = null;
        switch (a_direction)
        {
            case WallDirection.left:
            case WallDirection.right:
                currentWallInfos = a_floor.m_wallInfosHorz[a_subfloorIndex];
                break;
            case WallDirection.forward:
            case WallDirection.back:
                currentWallInfos = a_floor.m_wallInfosVert[a_subfloorIndex];
                break;
            default:
                return;
        }
        if (currentWallInfos.ContainsKey(a_index)) { currentWallInfos[a_index].AddWall(); }
        else
        {
            result.AddWall();
            currentWallInfos.Add(result.m_index, result);
        }
    }

    void RemoveWall(int a_index)
    {
        switch (m_currentDirection)
        {
            case WallDirection.left:
            case WallDirection.right:
                if (currentWallInfosHorz.ContainsKey(a_index)) { currentWallInfosHorz[a_index].RemoveWall(); }
                break;
            case WallDirection.forward:
            case WallDirection.back:
                if (currentWallInfosVert.ContainsKey(a_index)) { currentWallInfosVert[a_index].RemoveWall(); }
                break;
        }
    }

    private void ResetActiveWall()
    {
        if (!m_creatingWall) return;
        for (int j = m_currentIndices.Count - 1; j >= 0; j--)
        {
            bool previous = CurrentArrayHasWall(m_currentIndices[j]);
            if (m_currentMode == Mode.add) { if (!previous) { RemoveWall(m_currentIndices[j]); } }
            else { if (previous) { AddWall(m_currentIndices[j]); } }
        }
        m_currentIndices = new List<int>();
        m_lastConstructionPos = m_startPos;
    }

    private void RefreshAvailableTiles()
    {
        RefreshAvailableTiles(m_currentFloor, m_currentSubFloorIndex);
    }

    private void RefreshAvailableTiles(int a_floorIndex, int a_subFloorIndex)
    {
        RefreshAvailableTiles(LotManager.Instance.GetFloor(a_floorIndex), a_subFloorIndex);
    }

    private void RefreshAvailableTiles(Floor a_floor, int a_subFloorIndex)
    {
        a_floor.RefreshAvailableTilesWalls(a_subFloorIndex);
    }

    private List<bool> NewTiles()
    {
        // TODO: diagonal walls
        List<bool> result = new List<bool>();
        bool value = m_currentMode == Mode.add;
        switch (m_currentDirection)
        {
            case WallDirection.right:
            case WallDirection.left:
                result = new List<bool>(m_currentFloor.m_horizontalWallTiles[m_currentSubFloorIndex]);
                for (int i = 0; i < m_currentIndices.Count; i++) { result[m_currentIndices[i]] = value; }
                break;
            case WallDirection.forward:
            case WallDirection.back:
                result = new List<bool>(m_currentFloor.m_verticalWallTiles[m_currentSubFloorIndex]);
                for (int i = 0; i < m_currentIndices.Count; i++) { result[m_currentIndices[i]] = value; }
                break;
        }
        return result;
    }

    private void SetTiles(List<int> a_indices, bool a_newValue, WallDirection a_direction, int a_floorIndex, int a_subFloorIndex)
    {
        SetTiles(a_indices, a_newValue, a_direction, LotManager.Instance.GetFloor(a_floorIndex), a_subFloorIndex);
    }

    private void SetTiles(List<int> a_indices, bool a_newValue, WallDirection a_direction, Floor a_floor, int a_subFloorIndex)
    {
        // TODO: diagonal walls
        List<bool> wallTiles;
        switch (a_direction)
        {
            case WallDirection.right:
            case WallDirection.left:
                wallTiles = a_floor.m_horizontalWallTiles[a_subFloorIndex];
                break;
            case WallDirection.forward:
            case WallDirection.back:
                wallTiles = a_floor.m_verticalWallTiles[a_subFloorIndex];
                break;
            //TODO: diagonal
            default:
                return;
        }
        foreach (int i in a_indices) { wallTiles[i] = a_newValue; }
    }

    // TODO: make work with modular walls
    private void SetActiveWallMaterial(Material a_material)
    {
        //SetWallMaterial(m_activeWall, a_material);
    }

    // TODO: make work with modular walls
    private void SetWallMaterial(GameObject a_wall, Material a_material)
    {
        if (a_wall == null) { return; }
        MeshRenderer rendererComp = a_wall.GetComponentInChildren<MeshRenderer>();
        if (rendererComp != null)
        {
            Material[] materials = rendererComp.materials;
            materials[0] = a_material;
            rendererComp.materials = materials;
        }
    }
    #endregion

    public static Vector2Int ToPosition(int a_index, WallTool.WallDirection a_direction)
    {
        if (a_index < 0) { return new Vector2Int(-1, -1); }
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                return new Vector2Int(a_index % LotManager.width, a_index / LotManager.width);
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                return new Vector2Int(a_index % (LotManager.width + 1), a_index / (LotManager.width + 1));
            default:
                return new Vector2Int(-1, -1);
        }
    }

    public static int ToIndex(int a_x, int a_y, WallTool.WallDirection a_direction)
    {
        return ToIndex(new Vector2Int(a_x, a_y), a_direction);
    }

    public static int ToIndex(Vector2Int a_position, WallDirection a_direction)
    {
        if (a_position.x < 0 || a_position.y < 0) { return -1; }
        switch (a_direction)
        {
            case WallDirection.left:
            case WallDirection.right:
                return a_position.y * LotManager.width + a_position.x;
            case WallDirection.forward:
            case WallDirection.back:
                return a_position.y * (LotManager.width + 1) + a_position.x;
            default:
                return -1;
        }
    }

    public static bool IsValidPos(Vector2Int a_position, WallTool.WallDirection a_direction)
    {
        switch (a_direction)
        {
            case WallTool.WallDirection.left:
            case WallTool.WallDirection.right:
                return a_position.x >= 0 && a_position.x < LotManager.width && a_position.y >= 0 && a_position.y <= LotManager.length;
            case WallTool.WallDirection.forward:
            case WallTool.WallDirection.back:
                return a_position.x >= 0 && a_position.x <= LotManager.width && a_position.y >= 0 && a_position.y < LotManager.length;
            default:
                return false;
        }
    }
}
