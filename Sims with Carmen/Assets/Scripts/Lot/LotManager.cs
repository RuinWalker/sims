﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

// TODO: by default, load empty lot
public class LotManager : CachedBehaviour<LotManager>
{
    #region Variables
    #region Constants & Statics
    // TODO: should not be constant :')
    public const int width = 10;
    public const int length = 10;

    public static string path = "";
    #endregion

    #region Public
    public Floor groundFloor
    {
        get { return m_groundFloorComponent.floor; }
        set
        {
            m_groundFloorComponent = value.m_floorObjects[0].GetComponent<FloorComponent>();
        }
    }

    public Floor currentFloor { get; private set; }

    public Dictionary<int, List<int>> foundationTiles;

    public bool useAssignedFloor;
    #endregion

    #region Editor
    [SerializeField]
    private FloorComponent m_groundFloorComponent;

    [SerializeField]
    private Material terrainMaterial;
    #endregion
    #endregion

    #region Methods
    #region Unity
    private void Start()
    {
        foundationTiles = new Dictionary<int, List<int>>();
        if (useAssignedFloor)
        {
            m_groundFloorComponent.floor = new Floor(m_groundFloorComponent.gameObject);
            currentFloor = groundFloor;
        }
        else 
        {
            if (string.IsNullOrEmpty(path))
                path = MenuUIManager.CreatePathFrom(0, 0);
            string json = File.ReadAllText(path);
            LoadLot(json);
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Y))
#else
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.S))
#endif
            SaveLot();
    }
#endregion

#region Public
    /// <summary>
    /// Loads the lot from the JSON
    /// </summary>
    /// <param name="a_lotJson"></param>
    public void LoadLot(string a_lotJson)
    {
        JsonData jsonData = JsonMapper.ToObject(a_lotJson);
        LoadFloors(jsonData["floors"]);
        LoadFoundation(jsonData["foundation"]);
    }

    private void LoadFloors(JsonData a_jsonData)
    {
        Floor previousFloor = null;
        for (int i = 0; i < a_jsonData.Count; i++)
        {
            Floor.FloorHeader header = JsonMapper.ToObject<Floor.FloorHeader>(a_jsonData[i].ToJson());
            Floor floor = new Floor(header, previousFloor);
            previousFloor = floor;

            if (floor.index == 0)
            {
                // Set-up GroundFloor
                floor.m_floorFilters[0].mesh = FloorThings.MakeStraightFloor();
                FloorThings.Recalculate(floor.m_floorFilters[0].mesh, floor.m_floorColliders[0]);
                floor.m_floorObjects[0].GetComponent<MeshRenderer>().sharedMaterial = terrainMaterial;
                // Set LotManager GroundFloor
                Instance.groundFloor = floor;
                Instance.SetCurrentFloor(floor);
            }
        }
    }

    private void LoadFoundation(JsonData a_jsonData)
    {
        FoundationTool foundationTool = LotUIManager.Instance.foundationTool;
        Floor.FoundationHeader foundationHeader = JsonMapper.ToObject<Floor.FoundationHeader>(a_jsonData.ToJson());
        for (int i = 0; i < foundationHeader.subfloorIndices.Count; i++)
        {
            foundationTiles.Add(
                foundationHeader.subfloorIndices[i],
                foundationHeader.foundationTiles[i]
            );
            for (int j = 0; j < foundationHeader.foundationTiles[i].Count; j++)
            {
                foundationTool.AddFoundation(foundationHeader.foundationTiles[i][j]);
            }
        }
    }

    /// <summary>
    /// Saves the current lot to the file path for the current lot
    /// </summary>
    public void SaveLot()
    {
        Floor lowestFloor = groundFloor;
        while (lowestFloor.lowerFloor != null) { lowestFloor = lowestFloor.lowerFloor; }

        List<Floor.FloorHeader> floorHeaders = new List<Floor.FloorHeader>();
        while (lowestFloor != null)
        {
            floorHeaders.Add(Floor.FloorHeader.ToHeader(lowestFloor));
            lowestFloor = lowestFloor.higherFloor;
        }

        string floorsJson = JsonMapper.ToJson(floorHeaders);
        string foundationJson = JsonMapper.ToJson(Floor.FoundationHeader.ToHeader(foundationTiles));

        // String format doesn't work here 
        string finalJson = "{\"floors\":%0%, \"foundation\":%1%}";
        finalJson = finalJson.Replace("%0%", floorsJson);
        finalJson = finalJson.Replace("%1%", foundationJson);

        if (path != null)
            File.WriteAllText(path, finalJson);
        // TODO: if no path assigned, construct path
    }

    /// <summary>
    /// Get the floor at a_floorIndex, where the ground floor has index 0.
    /// </summary>
    /// <param name="a_floorIndex"></param>
    public Floor GetFloor(int a_floorIndex)
    {
        Floor currentFloor = groundFloor;
        if (a_floorIndex >= 0)
        {
            for (int i = 0; i < a_floorIndex; i++)
            {
                currentFloor = currentFloor.higherFloor;
                if (currentFloor == null)
                    break;
            }
        }
        else
        {
            for (int i = 0; i > a_floorIndex; i--)
            {
                currentFloor = currentFloor.lowerFloor;
                if (currentFloor == null)
                    break;
            }
        }
        return currentFloor;
    }

    // TODO: needs show/hide all floors between the current floor and the new floor
    public void SetCurrentFloor(Floor a_floor)
    {
        if (a_floor != null) { currentFloor = a_floor; }
        ChangeFloorState(a_floor, true);
    }

    public void FloorUp()
    {
        if (currentFloor.higherFloor != null)
        {
            currentFloor = currentFloor.higherFloor;
            ShowFloor(currentFloor);
        }
    }

    public void FloorDown()
    {
        if (currentFloor.lowerFloor != null)
        {
            HideFloor(currentFloor);
            currentFloor = currentFloor.lowerFloor;
        }
    }
#endregion

#region Private
    private void ShowFloor(Floor a_floor)
    {
        ChangeFloorState(a_floor, true);
    }

    private void HideFloor(Floor a_floor)
    {
        ChangeFloorState(a_floor, false);
    }

    private void ChangeFloorState(Floor a_floor, bool a_show)
    {
        foreach (GameObject gObj in a_floor.m_floorObjects) { gObj.SetActive(a_show); }
        foreach (GameObject gObj in a_floor.m_wallObjects) { gObj.SetActive(a_show); }
    }
#endregion
#endregion
}
