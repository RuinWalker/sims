﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: load furniture data
public class LotUIManager : CachedBehaviour<LotUIManager> {

    #region Variables
    #region Public

    public FoundationTool foundationTool { get { return m_foundationTool; } }
    #endregion

    #region Private
    [SerializeField]
    private LotManager m_lotManager;

    [SerializeField]
    private WallTool m_wallTool;

    [SerializeField]
    private FoundationTool m_foundationTool;
    #endregion
    #endregion

    #region Methods
    #region Unity
    private void Start()
    {
        m_lotManager = FindObjectOfType<LotManager>();

        m_wallTool = FindObjectOfType<WallTool>();
        m_foundationTool = FindObjectOfType<FoundationTool>();

        DisableAllBuildTools();
    }
    #endregion

    #region Actions
    public void ActionGoFloorUp()
    {
        m_lotManager.FloorUp();
    }

    public void ActionGoFloorDown()
    {
        m_lotManager.FloorDown();
    }

    public void ActionWallTool()
    {
        DisableAllBuildTools();
        m_wallTool.enabled = true;
    }

    public void ActionTileTool()
    {
        DisableAllBuildTools();
    }

    public void ActionFoundationTool()
    {
        DisableAllBuildTools();
        m_foundationTool.enabled = true;
    }

    public void ActionTerrainTool()
    {
        DisableAllBuildTools();
    }
    #endregion

    #region Private
    private void DisableAllBuildTools()
    {
        m_wallTool.enabled = false;

        m_foundationTool.enabled = false;
    }

    private void LoadFurniture()
    {
        foreach(KeyValuePair<int, FurnitureData> dataPair in FurnitureDataHolder.data)
        {
            // Create button
        }
    }
    #endregion
#endregion
}
