﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuUIManager : CachedBehaviour<MenuUIManager> {

    [SerializeField]
    private RectTransform m_networkGroup;

    [SerializeField]
    private RectTransform m_neighborhoodGroup;

    [SerializeField]
    private RectTransform m_lotGroup;

    [SerializeField]
    private RectTransform m_buttonsListRoot;

    [SerializeField]
    private RectTransform m_lotButtonsListRoot;

    [SerializeField]
    private GameObject m_neighborhoodButtonPrefab;

	void Start () {
        LoadNeighborhoods();
	}
	
    public void ActionToNeighborhoods()
    {
        m_networkGroup.gameObject.SetActive(false);
        m_neighborhoodGroup.gameObject.SetActive(true);
        m_lotGroup.gameObject.SetActive(false);
    }

    public void ActionToLots()
    {
        m_networkGroup.gameObject.SetActive(false);
        m_neighborhoodGroup.gameObject.SetActive(false);
        m_lotGroup.gameObject.SetActive(true);
    }

    private void LoadNeighborhoods()
    {
        for (int i = m_buttonsListRoot.childCount - 1; i >= 0; i--)
            Destroy(m_buttonsListRoot.GetChild(i).gameObject);

        if (!Directory.Exists(Application.persistentDataPath + "/Neighborhoods"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Neighborhoods");
        }
        string[] directories = Directory.GetDirectories(Application.persistentDataPath + "/Neighborhoods");
        foreach (string directoryPath in directories)
        {
            CreateNeighborhoodButton(directoryPath);
        }
    }

    private void CreateNeighborhoodButton(string a_path)
    {
        int id;
        {
            int index = a_path.LastIndexOf("\\");
            id = int.Parse(a_path.Remove(0, index + 1));
        }
        GameObject buttonObject = Instantiate(m_neighborhoodButtonPrefab, m_buttonsListRoot);
        buttonObject.GetComponentInChildren<Text>().text = id.ToString();
        buttonObject.GetComponent<Button>().onClick.AddListener(() => LoadLots(id, a_path));
    }

    private void LoadLots(int a_neighborhoodId, string a_path)
    {
        selectedNeighborhoodId = a_neighborhoodId;
        for (int i = m_lotButtonsListRoot.childCount - 1; i >= 0; i--)
            Destroy(m_lotButtonsListRoot.GetChild(i).gameObject);

        if (!Directory.Exists(a_path + "/Lots"))
        {
            Directory.CreateDirectory(a_path + "/Lots");
        }
        string[] files = Directory.GetFiles(a_path + "/Lots");
        foreach (string filePath in files)
        {
            CreateLotButton(filePath);
        }
        ActionToLots();
    }

    // TODO: this should go to neighbourhood screen
    private void CreateLotButton(string a_path)
    {
        int id;
        {
            int index = a_path.LastIndexOf("\\");
            id = int.Parse(a_path.Remove(0, index + 1));
        }
        GameObject buttonObject = Instantiate(m_neighborhoodButtonPrefab, m_lotButtonsListRoot);
        buttonObject.GetComponentInChildren<Text>().text = id.ToString();
        buttonObject.GetComponent<Button>().onClick.AddListener(() => ActionToLot(id, a_path));
    }

    public static string CreatePathFrom(int a_neighboorhoodId, int a_lotId)
    {
        string path = Application.persistentDataPath + "/Neighborhoods";
        if (!Directory.Exists(path)) { Directory.CreateDirectory(path); }
        path += "/" + a_neighboorhoodId;
        path += "/Lots";
        if (!Directory.Exists(path)) { Directory.CreateDirectory(path); }
        path += "/" + a_lotId;
        return path;
    }

    private void ActionToLot(int a_lotId, string a_path)
    {
        selectedLotId = a_lotId;
        LotManager.path = a_path;
        if (NetworkClient.active)
        {
            // Sync lot data
            long fileTime = File.GetLastWriteTimeUtc(a_path).ToFileTimeUtc();
            FindObjectOfType<PlayerController>().RpcSendServerDate(0, 0, fileTime, File.ReadAllText(a_path)); // TODO: make work with various neighborhood and lot indices
        }
        else
        {
            SceneManager.LoadScene("LotScene");
        }
    }

    public int selectedNeighborhoodId;
    public int selectedLotId;

}
