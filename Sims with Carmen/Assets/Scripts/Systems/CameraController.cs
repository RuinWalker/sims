﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[DisallowMultipleComponent]
public class CameraController : MonoBehaviour {

    [SerializeField]
    private float m_maxSpeed;
    [SerializeField]
    private float m_a;
    [SerializeField]
    private float m_accelTime;
    [SerializeField]
    private float m_decelTime;
    [SerializeField]
    private float m_rotationSpeed;

    private float m_t;

    private float m_currentSpeed;

    private Camera m_camera;

    private Vector3 m_lastMoveDir;

    private void Start()
    {
        m_camera = GetComponent<Camera>();
    }

    private void Update()
    {
        Vector2 moveDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
        bool inputMovement = moveDir.sqrMagnitude != 0;
        if (inputMovement) { m_lastMoveDir = moveDir; }
        UpdateSpeed(inputMovement);
        Truck(m_lastMoveDir.x);
        Dolly(m_lastMoveDir.y);
        if (Input.GetKey(KeyCode.Q))
            Pan(m_rotationSpeed);
        if (Input.GetKey(KeyCode.E))
            Pan(-m_rotationSpeed);
        // TODO: Y height based on floor height
    }

    private void UpdateSpeed(bool m_increaseSpeed)
    {
        m_t = Mathf.MoveTowards(m_t, m_increaseSpeed ? 1 : 0, Time.deltaTime * 1 / (m_increaseSpeed ? m_accelTime : m_decelTime));
        if (m_t == 1) { m_currentSpeed = m_maxSpeed; return; }
        if (m_t == 0) { m_currentSpeed = 0; return; }
        float toPowA = Mathf.Pow(m_t, m_a);
        float value = toPowA / (toPowA + Mathf.Pow(1 - m_t, m_a)); // x ^ a / (x ^ a + (1 - x) ^ a)
        m_currentSpeed = m_maxSpeed * value;
    }

    private void Truck(float a_value)
    {
        transform.position += transform.right * m_currentSpeed * a_value * Time.deltaTime;
    }

    private void Dolly(float a_value)
    {
        Vector3 axis = transform.forward;
        axis.y = 0;
        axis.Normalize();
        transform.position += axis * m_currentSpeed * a_value * Time.deltaTime;
    }

    private void Pan(float a_value)
    {
        RaycastHit hitInfo;
        Physics.Raycast(transform.position, transform.forward, out hitInfo, 1 << 8);
        transform.RotateAround(hitInfo.point, Vector3.up, a_value * Time.deltaTime);
    }
}
