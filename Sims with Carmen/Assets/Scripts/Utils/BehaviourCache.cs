﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// Holds Cache of (Unique) MonoBehaviours by Type
/// </summary>
public static class BehaviourCache
{
    /// <summary>
    /// Cache of Monobehaviours by Type
    /// </summary>
    private static readonly Dictionary<Type, MonoBehaviour> Cache = new Dictionary<Type, MonoBehaviour>();

    /// <summary>
    /// Returns (Cached) Behaviour of Type T. Uses FindObjectOfType if it is not currently in Cache
    /// </summary>
    /// <typeparam name="T">Type of Behaviour to return</typeparam>
    /// <returns>(Cached) Behaviour of Type T</returns>
    public static T FindBehaviourOfType<T>() where T : MonoBehaviour
    {
        if (!Cache.ContainsKey(typeof(T)) || Cache[typeof(T)] == null)
        {
            T obj = Object.FindObjectOfType<T>(); // Find (First) Object in Scene
            if (obj == null)
                return null;
            if (!Cache.ContainsKey(typeof(T)))
                Cache.Add(typeof(T), obj); // Add
            else
                Cache[typeof(T)] = obj; // Replace
        }
        return Cache[typeof(T)] as T; // Return
    }

    /// <summary>
    /// Registers a MonoBehaviour to the Cache
    /// </summary>
    /// <param name="behaviour">Behaviour to Register</param>
    /// <param name="forceReplace">Whether to force overriding existing Behaviours of same Type</param>
    /// <returns>True if registration is successfull</returns>
    public static bool Register<T>(T behaviour, bool forceReplace = false) where T : MonoBehaviour
    {
        Type t = behaviour.GetType();
        if (Cache.ContainsKey(t) && !ReferenceEquals(behaviour, Cache[t]))
        {
            if (forceReplace || Cache[t] == null)
            {
                if (Cache[t] != null)
                    if (t.BaseType != null && t.BaseType.IsGenericType && t.BaseType.GetGenericTypeDefinition().Equals(typeof(CachedBehaviour<>)))
                    {
                        Debug.LogWarning("Found existing CachedBehaviour. Destroying " + Cache[t].gameObject);
                        Object.DestroyImmediate(Cache[t].gameObject); // Destroy original CachedBehaviour (A CachedBehaviour MUST be unique)
                    }
                Cache[t] = behaviour; // Replace
                return true;
            }
            return false; // Object exists
        }
        else if (Cache.ContainsKey(t) && ReferenceEquals(behaviour, Cache[t]))
            return true;
        Cache.Add(t, behaviour); // Add
        return true;
    }
}