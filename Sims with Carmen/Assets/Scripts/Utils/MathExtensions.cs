﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtensions {

    public static float RoundToMultiple(float a_in, float a_multiple)
    {
        return a_multiple * Mathf.Round(a_in / a_multiple);
    }

    /// <summary>
    /// Check if a value is between two other values (inclusive).
    /// </summary>
    /// <param name="a_value"></param>
    /// <param name="a_lower"></param>
    /// <param name="a_upper"></param>
    /// <returns></returns>
    public static bool IsBetween(int a_value, int a_lower, int a_upper)
    {
        // TODO, check if range is valid
        return a_value >= a_lower && a_value <= a_upper;
    }

    // TODO, check if range is valid
    public static bool IsBetweenExclusive(int a_value, int a_lower, int a_upper)
    {
        return IsBetween(a_value, a_lower + 1, a_upper - 1);
    }

    public static bool IsBetween(float a_value, float a_lower, float a_upper)
    {
        return a_value >= a_lower && a_value <= a_upper;
    }

}
