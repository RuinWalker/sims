﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Conversion {

    public static string ToString(int[] a_array)
    {
        string arrayString = "";
        foreach (int index in a_array)
            arrayString += index.ToString() + ",";
        return arrayString;
    }

    public static string ToString(List<int> a_list)
    {
        return ToString(a_list.ToArray());
    }

}
