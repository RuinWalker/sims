﻿using UnityEngine;

/// <summary>
/// Adds this behaviour to BehaviourCache
/// If this behaviour is already Cached, the Script will be Destroyed (not the GameObject)
/// </summary>
public abstract class CachedBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    /// <summary>
    /// Overrides previously Cached Object of same Type on Awake
    /// </summary>
    [SerializeField]
    [Tooltip("Overrides previously Cached Object of same Type on Awake")]
    protected bool forceCacheReplace;

    /// <summary>
    /// Instance from the cache
    /// </summary>
    public static T Instance
    {
        get
        {
            return BehaviourCache.FindBehaviourOfType<T>();
        }
    }
    /// <summary>
    /// Registers Object to Cache
    /// </summary>
    protected virtual void Awake()
    {
        if (!BehaviourCache.Register(this, forceCacheReplace))
        {
            Debug.LogWarning("Cached Object already exists. Destroying new " + GetType().Name + " Instance on " + gameObject.name);
            Destroy(this); // Destroys Script only
        }
    }
}
